(function(a,b){if(typeof define==="function"&&define.amd){define(["d3"],b)
}else{if(typeof exports==="object"){module.exports=b(require("d3"))
}else{a.d3Kit=b(a.d3)
}}}(this,function(c){var g;
g=function(){var h=function(){function B(G,F,H){return G.on("click",F[H+"Click"]).on("mouseover",F[H+"MouseOver"]).on("mousemove",F[H+"MouseMove"]).on("mouseout",F[H+"MouseOut"])
}function p(G,F){if(F){return G.selectAll("*").remove()
}else{return G.selectAll("*").transition().style("opacity",0).remove()
}}function z(F){return typeof HTMLElement==="object"?F instanceof HTMLElement:F&&typeof F==="object"&&F!==null&&F.nodeType===1&&typeof F.nodeName==="string"
}var r=Number.isNaN?Number.isNaN:window.isNaN;
function n(F){return z(F)?F:document.querySelector(F)
}function v(F){return Array.isArray(F)?F:[].slice.call(document.querySelectorAll(F))
}function u(F){F=F||{};
for(var H=1;
H<arguments.length;
H++){var J=arguments[H];
if(!J){continue
}for(var G in J){if(J.hasOwnProperty(G)){var I=J[G];
if(s(I)&&!Array.isArray(I)&&!i(I)){F[G]=u(F[G],I)
}else{F[G]=I
}}}}return F
}function D(F){F=F||{};
for(var H=1;
H<arguments.length;
H++){if(!arguments[H]){continue
}for(var G in arguments[H]){if(arguments[H].hasOwnProperty(G)){F[G]=arguments[H][G]
}}}return F
}function t(F,G,H){if(F.addEventListener){F.addEventListener(G,H,false)
}else{if(F.attachEvent){F.attachEvent("on"+G,H)
}}}function E(F,G,H){F.removeEventListener(G,H,false)
}function l(G,J,F){var I;
var H=function(){var N=this,M=arguments;
var L=function(){I=null;
if(!F){G.apply(N,M)
}};
var K=F&&!I;
clearTimeout(I);
I=setTimeout(L,J);
if(K){G.apply(N,M)
}return N
};
H.isDebounced=true;
H.now=function(){clearTimeout(I);
return G.apply(this,arguments)
};
return H
}var q={"boolean":false,"function":true,object:true,number:false,string:false,"undefined":false};
function s(F){return !!(F&&q[typeof F])
}var y="[object Number]";
var w=Object.prototype;
var A=w.toString;
function x(F){return typeof F=="number"||F&&typeof F=="object"&&A.call(F)==y||false
}function i(F){var G={};
return !!F&&G.toString.call(F)==="[object Function]"
}var k=String.prototype.trim;
function m(F){if(F==null){return""
}return String(F).replace(/([.*+?^=!:${}()|[\]\/\\])/g,"\\$1")
}var o=function(F){if(F==null){return"\\s"
}else{if(F.source){return F.source
}else{return"["+m(F)+"]"
}}};
function C(G,F){if(G==null){return""
}if(!F&&k){return k.call(G)
}F=o(F);
return String(G).replace(new RegExp("^"+F+"+|"+F+"+$","g"),"")
}function j(F){return C(F).replace(/([A-Z])/g,"-$1").replace(/[-_\s]+/g,"-").toLowerCase()
}return{$:n,$$:v,dasherize:j,debounce:l,deepExtend:u,extend:D,isElement:z,isFunction:i,isNaN:r,isNumber:x,isObject:s,on:t,off:E,trim:C,removeAllChildren:p,bindMouseEventsToDispatcher:B}
}();
return h
}();
var f;
f=function(h){return function(i,q){var n={};
q=q||"g";
function k(r,s,u){var v=u?u+"."+s:s;
if(n.hasOwnProperty(v)){throw"invalid or duplicate layer id: "+v
}var t=r.append(q).classed(h.dasherize(s)+"-layer",true);
n[v]=t;
return t
}function m(r,v,u){if(Array.isArray(v)){return v.map(function(w){m(r,w,u)
})
}else{if(h.isObject(v)){var s=Object.keys(v)[0];
var t=k(r,s,u);
m(t,v[s],u?u+"."+s:s);
return t
}else{return k(r,v,u)
}}}function l(r){return m(i,r)
}function o(r){if(Array.isArray(r)){return r.map(l)
}else{return l(r)
}}function j(r){return n[r]
}function p(r){return !!n[r]
}return{create:o,get:j,has:p}
}
}(g);
var e;
e=function(i,h,j){var k={margin:{top:30,right:30,bottom:30,left:30},offset:[0.5,0.5],initialWidth:720,initialHeight:500};
var m=["data","options","resize"];
function l(t,W,K){var D={};
t=j.$(t);
var V=null;
var T=j.deepExtend({},k,W);
var M=0;
var N=0;
var J=0;
var C=0;
var z="window";
var G=false;
var aa=null;
var A=false;
var s=i.select(t).append("svg");
var R=s.append("g");
I();
var Z=new h(R);
var y=K?K.concat(m):m;
var x=i.dispatch.apply(i,y);
P([T.initialWidth,T.initialHeight]);
function U(ac,ab){if(arguments.length===0){return V
}V=ac;
if(!ab){x.data(ac)
}return D
}function E(ac,ab){if(arguments.length===0){return T
}T=j.deepExtend(T,ac);
if(ac){if(ac.margin){n(ab)
}else{if(ac.offset){I()
}}}if(!ab){x.options(ac)
}return D
}function I(){R.attr("transform","translate("+(T.margin.left+T.offset[0])+","+(T.margin.top+T.offset[1])+")")
}function n(ab){I();
J=M-T.margin.left-T.margin.right;
C=N-T.margin.top-T.margin.bottom;
if(!ab){x.resize([M,N,J,C])
}}function u(ac,ab){if(arguments.length===0){return T.margin
}T.margin=j.extend(T.margin,ac);
n(ab);
return D
}function O(ab){if(arguments.length===0){return T.offset
}T.offset=ab;
I();
return D
}function o(ac,ab){if(arguments.length===0||ac===null||ac===undefined){return M
}if(j.isNumber(ac)){M=+ac
}else{if(ac.trim().toLowerCase()=="auto"){M=t.clientWidth
}else{M=+(ac+"").replace(/px/gi,"").trim()
}}if(j.isNaN(M)){throw Error("invalid width: "+M)
}M=Math.floor(M);
J=M-T.margin.left-T.margin.right;
s.attr("width",M);
if(!ab){x.resize([M,N,J,C])
}return D
}function q(ac,ab){if(arguments.length===0||ac===null||ac===undefined){return N
}if(j.isNumber(ac)){N=+ac
}else{if(ac.trim().toLowerCase()=="auto"){N=t.clientHeight
}else{N=+(ac+"").replace(/px/gi,"").trim()
}}if(j.isNaN(N)){throw Error("invalid height: "+N)
}N=Math.floor(N);
C=N-T.margin.top-T.margin.bottom;
s.attr("height",N);
if(!ab){x.resize([M,N,J,C])
}return D
}function P(ab,ac){if(arguments.length===0){return[M,N]
}o(ab[0],true);
q(ab[1],ac);
return D
}function v(ab){if(arguments.length===0){return G
}else{if(G!=ab){return Q(ab,z)
}}return D
}function S(ab){if(arguments.length===0){return z
}else{if(z!=ab){return Q(G,ab)
}}return D
}function B(ab){if(arguments.length===0){return A
}if(ab===null||ab===undefined||ab===""||ab===false||(ab+"").toLowerCase()==="false"){A=false
}else{if(!j.isNumber(ab)){A=false
}else{if(+ab===0){A=false
}else{A=+ab
}}}return D
}function H(){if(aa){switch(z){case"dom":j.off(t,"resize",aa);
break;
default:case"window":j.off(window,"resize",aa);
break
}}aa=null;
return D
}function w(ab){if(ab){switch(z){case"dom":j.on(t,"resize",ab);
break;
default:case"window":j.on(window,"resize",ab);
break
}}aa=ab;
return D
}function Q(ad,ab){ad=ad&&(ad+"").toLowerCase()=="false"?false:ad;
ab=ab||z;
if(ad!=G){H();
G=ad;
z=ab;
if(ad){aa=j.debounce(function(){if(A){F(G,true);
r(A)
}else{F(G)
}},100);
w(aa)
}}else{if(ab!=z){var ac=aa;
H();
z=ab;
w(ac)
}}if(aa){aa()
}return D
}function Y(){return Object.keys(x).filter(function(ab){return m.indexOf(ab)<0
})
}function X(ab){var ac=D;
if(j.isObject(ab)){Object.keys(ab).forEach(function(ad){ac[ad]=ab[ad]
})
}return ac
}function F(ac,ab){switch(ac){case"all":case"full":case"both":P(["auto","auto"],ab);
break;
case"height":q("auto",ab);
break;
default:case"width":o("auto",ab);
break
}return D
}function r(ad,af){var ab=M;
var ac=N;
if(!j.isNumber(ad)){throw"Invalid ratio: must be a Number"
}ad=+ad;
if((ab/ac).toFixed(4)==ad.toFixed(4)){return D
}var ae=Math.floor(ab/ad);
if(ae>ac){o(Math.floor(ac*ad),af)
}else{q(ae,af)
}return D
}function L(){return V!==null&&V!==undefined
}function p(){return J>0&&C>0
}j.extend(D,{getCustomEventNames:Y,getDispatcher:function(){return x
},getInnerWidth:function(){return J
},getInnerHeight:function(){return C
},getLayerOrganizer:function(){return Z
},getRootG:function(){return R
},getSvg:function(){return s
},data:U,options:E,margin:u,offset:O,width:o,height:q,dimension:P,autoResize:v,autoResizeDetection:S,autoResizeToAspectRatio:B,hasData:L,hasNonZeroArea:p,mixin:X,resizeToFitContainer:F,resizeToAspectRatio:r});
i.rebind(D,x,"on");
return D
}return l
}(c,f,g);
var b;
b=function(j,i){var h=function(){function k(m,l,o){var n=function(q,p){var r=new j(q,i.deepExtend({},m,p),l);
if(o){o(r)
}return r
};
l=l?l:[];
n.getCustomEvents=function(){return l
};
return n
}return{createChart:k}
}();
return h
}(e,g);
var d;
d=function(i,j){function h(m,l){l()
}function k(t,r,o,s){r=r||h;
o=o||h;
s=s||[];
var n={};
var v=i.dispatch.apply(i,["enterDone","updateDone","exitDone"].concat(s));
function y(z,A){if(arguments.length>1){n[z]=i.functor(A);
return this
}return i.functor(n[z])
}function l(z,B,A){return y(z)(B,A)
}function m(A,z){return function(B){A(B,j.debounce(function(E,C){var D=v[z];
if(D){D(B)
}}),5)
}
}function w(z,B,A){n[A||B]=function(D,C){return z.property(B)(D,C)
};
return this
}function p(B,A,z){A.forEach(function(D,C){w(B,D,z&&C<z.length?z[C]:undefined)
});
return this
}function x(z){s.forEach(function(A){v.on(A,z[A])
});
return this
}function u(){return s
}var q={getDispatcher:function(){return v
},getPropertyValue:l,inheritPropertyFrom:w,inheritPropertiesFrom:p,publishEventsTo:x,getCustomEventNames:u,property:y,enter:m(t,"enterDone"),update:m(r,"updateDone"),exit:m(o,"exitDone")};
i.rebind(q,v,"on");
return q
}return k
}(c,g);
var a;
a=function(h,j,l,i,k){return{factory:h,helper:j,Skeleton:l,LayerOrganizer:i,Chartlet:k}
}(b,g,e,f,d);
return a
}));