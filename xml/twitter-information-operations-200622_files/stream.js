(function(){var d=["stream","bar","radial"];
TwitterAEM.components=TwitterAEM.components||{};
TwitterAEM.components.graphs=TwitterAEM.components.graphs||{};
function c(l,f,j,i,h){var k=$.Deferred(),g=TwitterAEM.components.graphs[h+"s"][h][f]||{};
l(i,function(m,n){if(m){k.reject(m)
}else{g.data=g.data||{};
g.data[j]={src:i,data:n};
TwitterAEM.components.graphs[h+"s"][h][f]=g;
k.resolve(n)
}});
return k
}function a(m,f,l,i,j){var h,k,g=[];
k=l.find(".js-data-sources").first().data();
for(h in k){if(k.hasOwnProperty(h)){g.push(c(d3.csv,f,h,k[h],j))
}}$.when.apply(null,g).done(function(){m.call(this,arguments,i)
}).fail(function(n){console.error(n)
})
}function b(f){TwitterAEM.components.graphs[f+"s"]=TwitterAEM.components.graphs[f+"s"]||{};
TwitterAEM.components.graphs[f+"s"][f]=TwitterAEM.components.graphs[f+"s"][f]||[];
if(typeof TwitterAEM.components.graphs[f+"s"].initData!=="function"){TwitterAEM.components.graphs[f+"s"].initData=function(){var g=Array.prototype.slice.call(arguments);
g.push(f);
return a.apply(this,g)
}
}}function e(h){var g,f;
for(f=h.length-1;
f>=0;
f-=1){b(h[f])
}}e(d)
}());
var SteamGraph;
(function(){var h=d3.scale.ordinal();
var b={margin:{top:0,right:0,bottom:0,left:0},offset:[0,0],duration:1000,areaInteroplate:"basis",colorOpacityRange:[0.1,0.7],colorFn:function a(l,k){return h(k)
},highlightColor:"white",stack:d3.layout.stack().offset("wiggle"),dividerFn:function e(l,k){return false
},dividerLabelFn:function f(l,k){return null
},xAccess:function g(k){return k.x
},yAccess:function j(k){return k.y
},idFn:function c(l,k){return k
},fixOffset:0};
var i=["layerMouseOver","layerMouseMove","layerMouseOut","layerClick"];
StreamGraph=d3Kit.factory.createChart(b,i,function d(p){var w=p.options();
var q=p.getDispatcher();
if(document.querySelectorAll(".wcmmode").length===0){p.autoResize("both").resizeToFitContainer("both").on("data",m).on("resize",v)
}else{p.resizeToFitContainer("both").on("data",m).on("resize",v)
}var u=p.getRootG();
var s=d3.scale.linear();
var r=d3.scale.linear();
var o=d3.scale.linear();
var k=d3.svg.area().interpolate(w.areaInteroplate).x(function(x){return s(w.xAccess(x))
}).y0(function(x){return r(x.y0)
}).y1(function(x){return r(x.y0+w.yAccess(x))
});
function m(){var y=p.data();
var x=d3.scale.linear().range(w.colorOpacityRange);
h.range(d3.range(y.length).map(function(A){var z="rgba(255, 255, 255, "+x(Math.random())+")";
if(y[A]&&y[A][0]&&y[A][0].color&&y[A][0].color.indexOf("#")<0){z="rgba("+y[A][0].color+", 0.6)"
}y[A].unhighlightColor=z;
return z
}));
v()
}function v(){if(!(p.hasData()&&p.hasNonZeroArea())){return
}var A=p.data();
var B=w.stack(A);
s.domain(d3.extent(B[0],function(D){return D.x
})).range([0,p.getInnerWidth()]);
r.domain([0,d3.max(B,function(D){return d3.max(D,function(E){return E.y0+w.yAccess(E)
})
})]).range([p.getInnerHeight(),0]);
o.domain(s.range()).range([0,A[0].length-1]);
var C=u.selectAll(".layer").data(B,w.idFn);
C.enter().append("path").classed("layer",true).on("mouseover",n(q.layerMouseOver)).on("mousemove",n(q.layerMouseMove)).on("mouseout",n(q.layerMouseOut)).on("click",n(q.layerClick)).style("fill",w.colorFn);
C.transition().duration(w.duration).attr("d",k);
C.exit().remove();
var z=B[0].filter(w.dividerFn);
var x=u.selectAll(".divider").data(z);
var y=x.enter().append("g").classed("divider",true);
y.append("line");
y.append("text");
x.attr("transform",function(D){return"translate("+[s(w.xAccess(D)+w.fixOffset()),0]+")"
});
x.select("line").attr("y1",r.range()[0]).attr("y2",r.range()[1]);
x.select("text").attr("y",r.range()[0]).attr("dy","1.3em").text(w.dividerLabelFn);
x.exit().remove();
if($(u[0]).length){$(u[0]).closest(".js-tr08").trigger("drawn")
}}function n(x){return function(z,y){x(z,y,z[Math.round(o(d3.event.offsetX))])
}
}function l(x){var z=x[0].color||w.highlightColor;
var y=z.indexOf("#")>=0;
if(!y){z="rgb("+z+")"
}u.selectAll(".layer").filter(function(A){return A==x
}).style("fill",z);
if(x[0].streamLink){u.selectAll(".layer").filter(function(A){return A==x
}).style("cursor","pointer")
}}function t(x){var y=p.data().indexOf(x);
u.selectAll(".layer").filter(function(z){return z==x
}).style("fill",function(z){return x.unhighlightColor
})
}return p.mixin({visualize:v,highlight:l,unhighlight:t})
})
}());
(function(d){var f="streamGraph",h=this,e,c;
function a(k){var l;
if(typeof k==="undefined"){k=Math.random().toString(36).substr(2,5)
}l=k.replace(/[_\W]+/g,"");
return l
}function i(l,k){h=this;
h.$wrapper=d(l);
h.id=a();
h.$wrapper.attr("data-graph-id",h.id);
h.data={};
h.methods={};
h.options=k||{};
h.init()
}function g(n,r,m,o,l){var k=Number(new Date())+(o||2000);
l=l||100;
(function q(){if(n()){r()
}else{if(Number(new Date())<k){setTimeout(q,l)
}else{m(new Error("timed out for "+n+": "+arguments))
}}}())
}function b(){var t,y={newGroups:[]};
function A(I){for(t=h.options.skipRows.length-1;
t>=0;
t-=1){if(I[h.options.skipRows[t].columnId]===h.options.skipRows[t].cellValue){return true
}}return false
}function E(I){if(I!==null&&I!==undefined){return I.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")
}}function B(I){I=parseInt(I,10);
if(isNaN(I)){I=0
}return I
}function w(K,I){var J=0,M=void 0,L={};
return K.map(function(O){if(A(O)){return null
}if(h.options.inverted){J+=1
}else{J-=1
}M=O[h.options.columns.streams[I].id];
L.streamId="stream_"+I;
L.streamLabel=h.options.columns.streams[I].label;
L.streamLink=h.options.columns.streams[I].href;
L.color=h.options.columns.streams[I].color;
L.sumLabel=h.options.columns.streams[I].sumLabel;
L.changeLabel=h.options.columns.streams[I].changeLabel;
L.showPercentage=h.options.columns.streams[I].showPercentage;
if(h.options.columns.groupBy){var N=O[h.options.columns.groupBy];
if(typeof N==="undefined"){console.error("One option references a column ("+h.options.columns.groupBy+") that doesn't exist in the provided CSV file.")
}L.groupId="group_"+a(N);
L.groupLabel=N
}L.itemValue=M;
L.dividerText=O[h.options.columns.dividerText];
return{x:J,y:M,color:L.color,dividerText:L.dividerText,graphId:h.id,groupId:L.groupId,groupLabel:L.groupLabel,itemValue:L.itemValue,streamId:L.streamId,streamLabel:L.streamLabel,showPercentage:L.showPercentage,streamSumLabel:L.sumLabel,streamChangeLabel:L.changeLabel,streamLink:L.streamLink}
})
}function o(I){if(typeof h.data.streams[I.streamId].groups[I.groupId].sum==="undefined"){h.data.streams[I.streamId].groups[I.groupId].sum=0;
h.data.streams[I.streamId].groups[I.groupId].count=0
}h.data.streams[I.streamId].groups[I.groupId].sum+=B(I.itemValue);
h.data.streams[I.streamId].groups[I.groupId].count++
}function F(I){if(I.y>h.data.streams.maxY){h.data.streams.maxY=I.y
}if(typeof h.data.streams[I.streamId].groups.maxY==="undefined"){h.data.streams[I.streamId].groups.maxY=I.y
}else{if(I.y>h.data.streams[I.streamId].groups.maxY){h.data.streams[I.streamId].groups.maxY=I.y
}}}function p(I){if(typeof h.data.streams[I.streamId].groups.sum==="undefined"){h.data.streams[I.streamId].groups.sum=0
}h.data.streams[I.streamId].groups.sum+=B(I.itemValue)
}function v(I){if(typeof h.data.streams[I.streamId]==="undefined"){h.data.streams[I.streamId]={}
}if(typeof h.data.streams[I.streamId].groups==="undefined"){h.data.streams[I.streamId].groups={}
}if(typeof h.data.streams[I.streamId].groups.count==="undefined"){h.data.streams[I.streamId].groups.count=0
}if(typeof h.data.streams[I.streamId].groups.groupIds==="undefined"){h.data.streams[I.streamId].groups.groupIds=[]
}if(typeof h.data.streams[I.streamId].groups[I.groupId]==="undefined"){h.data.streams[I.streamId].groups[I.groupId]={};
h.data.streams[I.streamId].groups.count+=1;
h.data.streams[I.streamId].groups.groupIds.push(I.groupId)
}o(I);
F(I);
p(I);
if(h.data.streams[I.streamId].groups.count>h.data.streams.mostGroupsInStream){h.data.streams.mostGroupsInStream=h.data.streams[I.streamId].groups.count
}}function n(I){return I.filter(function(K,J){if(K===null){return false
}K.y=B(K.y);
v(K);
return K
})
}function k(J){var I=h.data.streams.maxY;
J=J.map(function(L){var K=h.data.streams[L.streamId].groups.maxY,M=void 0;
M=L.y;
L.y=M;
return L
});
return J
}function D(I){I=I.map(function(J){return k(J)
});
return I
}function m(K){var L=[],J,I;
for(J=h.options.columns.streams.length-1;
J>=0;
J-=1){I=w(K,J);
I=n(I);
h.data.streams.count+=1;
L.push(I)
}L=D(L);
return L
}function G(L,J){var I,K;
if(J.x>0){I=J.x-1
}else{I=L.length-Math.abs(J.x)
}K=L[I];
return K
}function z(Q,T,R){var S=G(Q,R),L,M,U,I,J,P,O;
if(R.graphId!==e){e=R.graphId;
h=d('[data-graph-id="'+R.graphId+'"]').data("plugin_"+f)
}L=h.data.streams[S.streamId].groups.sum;
M=h.data.streams[S.streamId].groups[S.groupId].sum;
U=h.data.streams[S.streamId].groups[S.groupId].count;
I=parseInt(M/U);
P=function P(){var W=h.data.streams[S.streamId].groups.groupIds,X=W.indexOf(S.groupId),Y,V;
if(X<W.length-1){Y=X+1;
V=W[Y]
}else{return undefined
}return h.data.streams[S.streamId].groups[V].sum
};
J=function J(){var X=M,W,V="";
W=X-P();
if(W>0){V="+"
}else{if(isNaN(W)){return"‑‑"
}}return V+W
};
O={streamLabel:S.streamLabel,streamLink:S.streamLink,streamSum:L,groupLabel:S.groupLabel,groupSum:M,groupChange:J(),itemValue:S.itemValue,streamSumLabel:S.streamSumLabel,streamChangeLabel:S.streamChangeLabel,showPercentage:S.showPercentage,streamTotalPercentage:I,dividerText:S.dividerText};
function N(W,X){var V=h.options.$infoBox.find(W);
V.text(X)
}function K(X,W){var V=h.options.$infoBox.find(X);
V.attr("href",W)
}N(h.options.cssSelectors.streamLabel,O.streamLabel);
N(h.options.cssSelectors.streamSumLabel,O.streamSumLabel);
N(h.options.cssSelectors.streamChangeLabel,O.streamChangeLabel);
N(h.options.cssSelectors.groupLabel,O.groupLabel);
N(h.options.cssSelectors.groupSum,E(O.groupSum));
if(O.showPercentage==="true"){N(h.options.cssSelectors.groupSum,E(O.streamTotalPercentage))
}N(h.options.cssSelectors.groupChange,E(O.groupChange));
K(h.options.cssSelectors.streamLink,O.streamLink)
}function r(K,I,L){var J;
if(y.newGroups.length===0){for(J=L.length;
J>=0;
J-=1){if(L[J+1]){if(L[J].groupId!==L[J+1].groupId){y.newGroups.push(J)
}}}}if(d.inArray(I,y.newGroups)!==-1){return true
}}function s(){var I=h.$wrapper;
h.options.infoBox=d3.select(I.find(".info-box")[0]);
h.options.canvas=I.find(".stream-graph")[0];
h.options.$streamConfig=I.find(".js-data-config");
h.options.streamColumns=h.options.$streamConfig.find(".js-stream").toArray().map(function(K){var J=d(K);
return{color:J.attr("data-highlight-color"),href:J.attr("data-link-url"),id:J.attr("data-column"),showPercentage:J.attr("data-show-percentage"),sumLabel:J.attr("data-label-sum"),changeLabel:J.attr("data-label-change"),label:J.attr("data-title")}
});
h.options.skipRows=h.options.$streamConfig.find(".js-skip-row").toArray().map(function(K){var J=d(K);
return{columnId:J.attr("data-column"),cellValue:J.attr("data-cell-value")}
});
h.options.inverted=h.options.$streamConfig.attr("data-invert");
h.options.highlightColor=h.options.$streamConfig.attr("data-highlight-color");
h.options.columns={dividerText:h.options.$streamConfig.attr("data-divider-text"),groupBy:h.options.$streamConfig.attr("data-groups"),streams:h.options.streamColumns};
h.options.cssSelectors={streamLabel:".js-streamLabel",streamSumLabel:".js-streamSumLabel",streamChangeLabel:".js-streamChangeLabel",streamLink:".js-link",groupLabel:".js-groupLabel",groupSum:".js-groupSum",groupChange:".js-groupChange"};
h.data={streams:{count:0,mostGroupsInStream:0,maxY:null}};
h.options.infoBoxContainer=h.options.infoBox.node().parentNode;
h.options.$infoBox=d(h.options.infoBox.node());
h.options.infoBoxWidth=h.options.$infoBox.outerWidth()
}function H(){h.options.infoBox.style("display","block")
}function x(){h.options.infoBox.style("display","none")
}function q(K){var N,I,J,M,L;
N={x:d3.mouse(h.options.infoBoxContainer)[0],y:d3.mouse(h.options.infoBoxContainer)[1]};
I=d(h.options.infoBoxContainer);
J=I.outerWidth();
M=N.x-parseInt(h.options.infoBox.style("width"),10)/2;
L=N.y-parseInt(h.options.infoBox.style("height"),10)-10;
if(N.x-h.options.infoBoxWidth/2<I.offset().left){M=0
}else{if(N.x+h.options.infoBoxWidth/2>J){M=J-h.options.infoBoxWidth
}}if(L<0){L=0
}h.options.infoBox.style("left",M+"px").style("top",L+"px")
}function u(I){H();
q(I)
}function C(K,J,I){h.options.$infoBox.one("mouseleave",function(){x();
K.unhighlight(J)
})
}function l(M,L,I,K){function O(){return c!==I||h.options.$infoBox.is(":hover")
}function N(){if(c===I){C(M,L,K)
}else{M.unhighlight(L)
}}function J(){if(c===I&&!h.options.$infoBox.is(":hover")){x()
}M.unhighlight(L)
}g(O,N,J,1000,30)
}h.methods.createGraph=function(K){var O,N,I,M;
h=K;
h.$wrapper.addClass("initialized");
s();
O=TwitterAEM.components.graphs.streams.stream[h.id].data.source.data;
N=m(O);
I={margin:{top:17,bottom:25,left:0,right:0},dividerFn:r,dividerLabelFn:function J(Q,P){return Q.dividerText
},highlightColor:h.options.highlightColor,fixOffset:function L(){return h.options.inverted?0.5:-0.5
}};
h.options.Graph=d.extend(true,I,h.options.Graph);
M=new StreamGraph(h.options.canvas,h.options.Graph);
M.on("layerMouseOver",function(R,P,Q){c=P;
M.highlight(R);
z(R,P,Q);
u(Q)
}).on("layerMouseMove",function(R,P,Q){z(R,P,Q);
q(Q)
}).on("layerMouseOut",function(R,P,Q){x();
M.unhighlight(R)
}).on("layerClick",function(R,Q){var P=h.$wrapper.find(h.options.cssSelectors.streamLink)[0];
if(P.href!=window.location){P.click()
}});
M.data(N)
};
TwitterAEM.components.graphs.streams.init=function(I,K,J){TwitterAEM.components.graphs.streams.initData(function(M,L){L.methods.createGraph(L)
},I,K,J)
}
}d.extend(i.prototype,{init:function j(){b();
TwitterAEM.components.graphs.streams.init(h.id,h.$wrapper,h)
}});
d.fn[f]=function(k){return this.each(function(){if(!d.data(this,"plugin_"+f)){d.data(this,"plugin_"+f,new i(this,k))
}})
}
}(jQuery));
$(function(){$(".js-tr08:not(.initialized):not(.js-tr08-masthead)").each(function(){var a=$(this);
if(a.find(".js-graphs-tabs-placeholder").height()){a.streamGraph()
}})
});
$(function(){var b=$(".masthead .copy-container").first().outerHeight();
function c(g,j,f,h,e){var d=Number(new Date())+(h||2000);
e=e||100;
(function i(){if(g()){j()
}else{if(Number(new Date())<d){setTimeout(i,e)
}else{f(new Error("Timed out for "+g+": "+arguments))
}}}())
}function a(){$(".masthead").each(function(){var e=$(this);
function d(){var g,h,f,i;
g=e.find(".copy-container").outerHeight();
f=parseInt($("body").css("font-size").replace("px",""),10);
h=f*3;
i=f*2;
return g+h+i
}e.find(".js-tr08:not(.initialized)").streamGraph({Graph:{margin:{bottom:d(),top:$(".transparency-navigation").outerHeight()-10}}})
});
if($(".js-tr08-masthead").length>0){$("body").addClass("transparency-masthead-loaded")
}}c(function(){return $(".masthead .copy-container").first().outerHeight()!==b
},function(){a()
},function(){a()
},400,10)
});