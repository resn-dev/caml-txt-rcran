(function(){var d=["stream","bar","radial"];
TwitterAEM.components=TwitterAEM.components||{};
TwitterAEM.components.graphs=TwitterAEM.components.graphs||{};
function c(l,f,j,i,h){var k=$.Deferred(),g=TwitterAEM.components.graphs[h+"s"][h][f]||{};
l(i,function(m,n){if(m){k.reject(m)
}else{g.data=g.data||{};
g.data[j]={src:i,data:n};
TwitterAEM.components.graphs[h+"s"][h][f]=g;
k.resolve(n)
}});
return k
}function a(m,f,l,i,j){var h,k,g=[];
k=l.find(".js-data-sources").first().data();
for(h in k){if(k.hasOwnProperty(h)){g.push(c(d3.csv,f,h,k[h],j))
}}$.when.apply(null,g).done(function(){m.call(this,arguments,i)
}).fail(function(n){console.error(n)
})
}function b(f){TwitterAEM.components.graphs[f+"s"]=TwitterAEM.components.graphs[f+"s"]||{};
TwitterAEM.components.graphs[f+"s"][f]=TwitterAEM.components.graphs[f+"s"][f]||[];
if(typeof TwitterAEM.components.graphs[f+"s"].initData!=="function"){TwitterAEM.components.graphs[f+"s"].initData=function(){var g=Array.prototype.slice.call(arguments);
g.push(f);
return a.apply(this,g)
}
}}function e(h){var g,f;
for(f=h.length-1;
f>=0;
f-=1){b(h[f])
}}e(d)
}());
(function(g){var f="switchChartsFromDropdown",e=this;
function d(j,i){e=this;
e.$wrapper=g(j);
e.options={classInputSelect:"js-select-value",attrIndicator:"data-dropdown-indicator",callback:null};
g.extend(e.options,i);
e.init()
}function c(k,j){var i=Array.prototype.slice.call(arguments);
i.push(j);
i.shift();
i.shift();
return k.apply(this,i)
}function b(i,j){j.$wrapper.find("["+j.options.attrIndicator+"]").each(function(){var k=g(this);
if(k.attr(j.options.attrIndicator)===i.target.value){k.removeAttr("hidden");
if(typeof j.options.callback==="function"){j.options.callback(k)
}else{throw"No callback provided."
}}else{k.attr("hidden","hidden")
}})
}function a(){e.$wrapper.find("."+e.options.classInputSelect).on("change",c.bind(undefined,b,e))
}g.extend(d.prototype,{init:function h(){a()
}});
g.fn[f]=function(i){return this.each(function(){if(!g.data(this,"plugin_"+f)){g.data(this,"plugin_"+f,new d(this,i))
}})
}
}(jQuery));
var RadialChart;
(function(){var e=d3.scale.ordinal();
var g={margin:{top:110,right:0,bottom:50,left:0},offset:[0,0],thickness:70,radius:167,percentFormat:d3.format("%"),ofFormat:d3.format(","),ofRise:0.15,dotRadius:12,dotRise:0.45,dotRun:0.3,title:"The Title of This Chart",colorOpacityRange:[0.03,0.7],valueAccess:function a(h){return h.value
},detailAaccess:function f(h){return h.detail
},colorFn:function c(h,i){return e(h.startAngle)
}};
var b=["sliceMouseOver","sliceMouseMove","sliceMouseOut","sliceClick"];
RadialChart=d3Kit.factory.createChart(g,b,function d(l){var w=l.options();
var r=l.getDispatcher();
if(document.querySelectorAll(".wcmmode").length===0){l.autoResize("both").resizeToFitContainer("both").on("data",v).on("resize",v)
}else{l.resizeToFitContainer("both").on("data",v).on("resize",v)
}var j=d3.svg.arc();
var n=d3.layout.pie().sort(null).value(w.valueAccess);
var t=l.getRootG();
var k=t.append("g");
var s=t.append("text").classed("title",true).style("text-anchor","middle").text(w.title);
var i=k.append("circle").classed("dot",true);
var q=k.append("text").classed("percent",true).style("text-anchor","middle").attr("dy","0.32em");
var u=k.append("text").classed("of",true).style("text-anchor","middle").attr("dy","0.32em");
var h=k.append("foreignObject");
var o=h.append("xhtml:body").style("background","none").append("p").classed("detail",true).attr("align","center");
p();
function v(){if(!(l.hasData()&&l.hasNonZeroArea())){return
}var A=l.data();
var x=l.getInnerWidth();
var z=l.getInnerHeight();
var y=d3.scale.linear().domain([0,A.length-1]).range(w.colorOpacityRange);
e.range(d3.range(A.length).map(function(C){return"rgba(0, 95, 209, "+y(C)+")"
}));
s.attr("x",x/2).attr("y",-w.margin.top/3);
j.outerRadius(w.radius).innerRadius(w.radius-w.thickness);
i.attr("cx",(w.radius-w.thickness+20)*-w.dotRun).attr("cy",(w.radius-w.thickness)*-w.dotRise).attr("r",w.dotRadius);
q.attr("x",(w.radius-w.thickness-20)*w.dotRun).attr("y",(w.radius-w.thickness)*-w.dotRise);
u.attr("y",(w.radius-w.thickness)*-w.ofRise).text("OF "+w.ofFormat(d3.sum(l.data(),w.valueAccess)));
h.attr("x",-(w.radius-w.thickness)).attr("width",(w.radius-w.thickness)*2).attr("height",w.radius-w.thickness);
k.attr("transform","translate("+[x/2,z/2]+")");
var B=k.selectAll(".arc").data(n(A));
B.enter().append("path").classed("arc",true).call(d3Kit.helper.bindMouseEventsToDispatcher,l.getDispatcher(),"slice",r);
B.exit().remove();
B.attr("d",j).style("fill",function(D,C){return w.colorFn(D,C)
})
}function m(x){i.style("display","block");
u.style("display","block");
i.style("fill",function(){return w.colorFn(x)
});
q.text(w.percentFormat(w.valueAccess(x.data)/d3.sum(l.data(),w.valueAccess)));
o.text(w.detailAaccess(x.data))
}function p(){i.style("display","none");
u.style("display","none");
o.text("");
q.text("")
}return l.mixin({pie:n,visualize:v,highlightSlice:m,unhighlightSlice:p})
})
}());
(function(g){var b="radialchart",l=this,f={};
function c(s){var t;
if(s===undefined){s=Math.random().toString(36).substr(2,5)
}t=s.replace(/[_\W]+/g,"");
return t
}function h(s){return Granite.I18n.get(s)
}function d(t,s){l=this;
l.id=c();
l.$wrapper=g(t);
l.$chart=l.$wrapper.find(".js-radial-graph");
l.options={};
g.extend(l.options,s);
l.init()
}function e(t,s){s.data.detail=h(s.data.detail);
t.radialChart.visualize();
t.radialChart.highlightSlice(s)
}function a(s){e(this,s)
}function i(s){var t=this;
t.radialChart.unhighlightSlice(s);
n(t)
}function p(s,u){var t;
for(t in s){if(s.hasOwnProperty(t)){u(s[t],t)
}}}function m(s){return g.map(s,function(t){return t
})
}function k(t,s){p(t,function(v,u){p(v,function(x,w){if(s.options.column!==w){return
}if(f[w]===undefined){f[w]={}
}if(f[w][x]===undefined){f[w][x]={value:1,detail:s.options.attrData[x]}
}else{f[w][x].value+=1
}})
})
}function r(t,s){t.radialChart.data(s);
t.radialChart.pieData=t.radialChart.pie(s)
}function n(t){var s=t.radialChart.pieData.reduce(function(u,v){return parseInt(u.value,10)>parseInt(v.value,10)?u:v
});
e(t,s)
}function o(t,s){s.options.title=s.$wrapper.data("title");
s.options.column=s.$wrapper.data("column");
s.options.attrData={};
s.$wrapper.find(".js-data-segment").each(function(){var u=g(this).data();
s.options.attrData[u.segmentValue]=u.segmentText
});
s.radialChart=new RadialChart(s.$chart[0],{title:s.options.title}).on("sliceMouseOver",a.bind(s)).on("sliceMouseOut",i.bind(s));
t=g.map(t.domains,function(u){return{verified:u.counts.verified,security:u.counts.rating}
});
k(t,s);
t=f[s.options.column];
t=m(t);
r(s,t);
n(s)
}function j(t,s){o(t,s)
}function q(s){s=s.replace("data = ","");
return JSON.parse(s)
}g.extend(d.prototype,{init:function o(){var s=l;
g.ajax({cache:true,dataType:"text",method:"GET",url:l.$wrapper.find(".js-data-sources").first().data("source")}).done(function(u){var t=q(u);
j(t,s)
})
}});
g.fn[b]=function(s){return this.each(function(){if(!g.data(this,"plugin_"+b)){g.data(this,"plugin_"+b,new d(this,s))
}})
}
}(jQuery));
(function(){$(".js-tr11  .js-graph").each(function(a){$(this).radialchart()
});
$(".js-tr11").switchChartsFromDropdown({callback:function(a){a.radialchart()
}})
}());