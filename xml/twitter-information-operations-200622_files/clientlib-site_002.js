/*!
 * ZeroClipboard
 * The ZeroClipboard library provides an easy way to copy text to the clipboard using an invisible Adobe Flash movie and a JavaScript interface.
 * Copyright (c) 2009-2014 Jon Rohan, James M. Greene
 * Licensed MIT
 * http://zeroclipboard.org/
 * v2.2.0
 */
;
!function(a7,a6){var a5,a4,a1,a0=a7,aZ=a0.document,aY=a0.navigator,aX=a0.setTimeout,aU=a0.clearTimeout,aT=a0.setInterval,aR=a0.clearInterval,aQ=a0.getComputedStyle,aP=a0.encodeURIComponent,aN=a0.ActiveXObject,aM=a0.Error,aK=a0.Number.parseInt||a0.parseInt,aJ=a0.Number.parseFloat||a0.parseFloat,aI=a0.Number.isNaN||a0.isNaN,aH=a0.Date.now,aG=a0.Object.keys,aF=a0.Object.defineProperty,aE=a0.Object.prototype.hasOwnProperty,aD=a0.Array.prototype.slice,aC=function(){var f=function(b){return b
};
if("function"==typeof a0.wrap&&"function"==typeof a0.unwrap){try{var e=aZ.createElement("div"),h=a0.unwrap(e);
1===e.nodeType&&h&&1===h.nodeType&&(f=a0.unwrap)
}catch(g){}}return f
}(),aA=function(b){return aD.call(b,0)
},bM=function(){var b,p,o,n,m,l,k=aA(arguments),j=k[0]||{};
for(b=1,p=k.length;
p>b;
b++){if(null!=(o=k[b])){for(n in o){aE.call(o,n)&&(m=j[n],l=o[n],j!==l&&l!==a6&&(j[n]=l))
}}}return j
},bK=function(g){var f,j,i,h;
if("object"!=typeof g||null==g||"number"==typeof g.nodeType){f=g
}else{if("number"==typeof g.length){for(f=[],j=0,i=g.length;
i>j;
j++){aE.call(g,j)&&(f[j]=bK(g[j]))
}}else{f={};
for(h in g){aE.call(g,h)&&(f[h]=bK(g[h]))
}}}return f
},bJ=function(g,f){for(var j={},i=0,h=f.length;
h>i;
i++){f[i] in g&&(j[f[i]]=g[f[i]])
}return j
},bI=function(f,e){var h={};
for(var g in f){-1===e.indexOf(g)&&(h[g]=f[g])
}return h
},bG=function(d){if(d){for(var c in d){aE.call(d,c)&&delete d[c]
}}return d
},bD=function(d,c){if(d&&1===d.nodeType&&d.ownerDocument&&c&&(1===c.nodeType&&c.ownerDocument&&c.ownerDocument===d.ownerDocument||9===c.nodeType&&!c.ownerDocument&&c===d.ownerDocument)){do{if(d===c){return !0
}d=d.parentNode
}while(d)
}return !1
},bC=function(d){var c;
return"string"==typeof d&&d&&(c=d.split("#")[0].split("?")[0],c=d.slice(0,d.lastIndexOf("/")+1)),c
},bB=function(e){var d,f;
return"string"==typeof e&&e&&(f=e.match(/^(?:|[^:@]*@|.+\)@(?=http[s]?|file)|.+?\s+(?: at |@)(?:[^:\(]+ )*[\(]?)((?:http[s]?|file):\/\/[\/]?.+?\/[^:\)]*?)(?::\d+)(?::\d+)?/),f&&f[1]?d=f[1]:(f=e.match(/\)@((?:http[s]?|file):\/\/[\/]?.+?\/[^:\)]*?)(?::\d+)(?::\d+)?/),f&&f[1]&&(d=f[1]))),d
},bA=function(){var e,d;
try{throw new aM
}catch(f){d=f
}return d&&(e=d.sourceURL||d.fileName||bB(d.stack)),e
},bz=function(){var b,f,e;
if(aZ.currentScript&&(b=aZ.currentScript.src)){return b
}if(f=aZ.getElementsByTagName("script"),1===f.length){return f[0].src||a6
}if("readyState" in f[0]){for(e=f.length;
e--;
){if("interactive"===f[e].readyState&&(b=f[e].src)){return b
}}}return"loading"===aZ.readyState&&(b=f[f.length-1].src)?b:(b=bA())?b:a6
},bw=function(){var b,h,g,f=aZ.getElementsByTagName("script");
for(b=f.length;
b--;
){if(!(g=f[b].src)){h=null;
break
}if(g=bC(g),null==h){h=g
}else{if(h!==g){h=null;
break
}}}return h||a6
},bv=function(){var b=bC(bz())||bw()||"";
return b+"ZeroClipboard.swf"
},bt=function(){return null==a7.opener&&(!!a7.top&&a7!=a7.top||!!a7.parent&&a7!=a7.parent)
}(),bs={bridge:null,version:"0.0.0",pluginType:"unknown",disabled:null,outdated:null,sandboxed:null,unavailable:null,degraded:null,deactivated:null,overdue:null,ready:null},br="11.0.0",bq={},bo={},bm=null,bl=0,bk=0,bj={ready:"Flash communication is established",error:{"flash-disabled":"Flash is disabled or not installed. May also be attempting to run Flash in a sandboxed iframe, which is impossible.","flash-outdated":"Flash is too outdated to support ZeroClipboard","flash-sandboxed":"Attempting to run Flash in a sandboxed iframe, which is impossible","flash-unavailable":"Flash is unable to communicate bidirectionally with JavaScript","flash-degraded":"Flash is unable to preserve data fidelity when communicating with JavaScript","flash-deactivated":"Flash is too outdated for your browser and/or is configured as click-to-activate.\nThis may also mean that the ZeroClipboard SWF object could not be loaded, so please check your `swfPath` configuration and/or network connectivity.\nMay also be attempting to run Flash in a sandboxed iframe, which is impossible.","flash-overdue":"Flash communication was established but NOT within the acceptable time limit","version-mismatch":"ZeroClipboard JS version number does not match ZeroClipboard SWF version number","clipboard-error":"At least one error was thrown while ZeroClipboard was attempting to inject your data into the clipboard","config-mismatch":"ZeroClipboard configuration does not match Flash's reality","swf-not-found":"The ZeroClipboard SWF object could not be loaded, so please check your `swfPath` configuration and/or network connectivity"}},bi=["flash-unavailable","flash-degraded","flash-overdue","version-mismatch","config-mismatch","clipboard-error"],bh=["flash-disabled","flash-outdated","flash-sandboxed","flash-unavailable","flash-degraded","flash-deactivated","flash-overdue"],bg=new RegExp("^flash-("+bh.map(function(b){return b.replace(/^flash-/,"")
}).join("|")+")$"),bf=new RegExp("^flash-("+bh.slice(1).map(function(b){return b.replace(/^flash-/,"")
}).join("|")+")$"),be={swfPath:bv(),trustedDomains:a7.location.host?[a7.location.host]:[],cacheBust:!0,forceEnhancedClipboard:!1,flashLoadTimeout:30000,autoActivate:!0,bubbleEvents:!0,containerId:"global-zeroclipboard-html-bridge",containerClass:"global-zeroclipboard-container",swfObjectId:"global-zeroclipboard-flash-bridge",hoverClass:"zeroclipboard-is-hover",activeClass:"zeroclipboard-is-active",forceHandCursor:!1,title:null,zIndex:999999999},bY=function(d){if("object"==typeof d&&null!==d){for(var c in d){if(aE.call(d,c)){if(/^(?:forceHandCursor|title|zIndex|bubbleEvents)$/.test(c)){be[c]=d[c]
}else{if(null==bs.bridge){if("containerId"===c||"swfObjectId"===c){if(!bE(d[c])){throw new Error("The specified `"+c+"` value is not valid as an HTML4 Element ID")
}be[c]=d[c]
}else{be[c]=d[c]
}}}}}}if("string"!=typeof d||!d){return bK(be)
}if(aE.call(be,d)){return be[d]
}},ba=function(){return b2(),{browser:bJ(aY,["userAgent","platform","appName"]),flash:bI(bs,["bridge"]),zeroclipboard:{version:bd.version,config:bd.config()}}
},bR=function(){return !!(bs.disabled||bs.outdated||bs.sandboxed||bs.unavailable||bs.degraded||bs.deactivated)
},bp=function(b,l){var k,j,i,c={};
if("string"==typeof b&&b){i=b.toLowerCase().split(/\s+/)
}else{if("object"==typeof b&&b&&"undefined"==typeof l){for(k in b){aE.call(b,k)&&"string"==typeof k&&k&&"function"==typeof b[k]&&bd.on(k,b[k])
}}}if(i&&i.length){for(k=0,j=i.length;
j>k;
k++){b=i[k].replace(/^on/,""),c[b]=!0,bq[b]||(bq[b]=[]),bq[b].push(l)
}if(c.ready&&bs.ready&&bd.emit({type:"ready"}),c.error){for(k=0,j=bh.length;
j>k;
k++){if(bs[bh[k].replace(/^flash-/,"")]===!0){bd.emit({type:"error",name:bh[k]});
break
}}a5!==a6&&bd.version!==a5&&bd.emit({type:"error",name:"version-mismatch",jsVersion:bd.version,swfVersion:a5})
}}return bd
},aO=function(i,h){var n,m,l,k,j;
if(0===arguments.length){k=aG(bq)
}else{if("string"==typeof i&&i){k=i.split(/\s+/)
}else{if("object"==typeof i&&i&&"undefined"==typeof h){for(n in i){aE.call(i,n)&&"string"==typeof n&&n&&"function"==typeof i[n]&&bd.off(n,i[n])
}}}}if(k&&k.length){for(n=0,m=k.length;
m>n;
n++){if(i=k[n].toLowerCase().replace(/^on/,""),j=bq[i],j&&j.length){if(h){for(l=j.indexOf(h);
-1!==l;
){j.splice(l,1),l=j.indexOf(h,l)
}}else{j.length=0
}}}}return bd
},ar=function(d){var c;
return c="string"==typeof d&&d?bK(bq[d])||null:bK(bq)
},ah=function(f){var e,h,g;
return f=a2(f),f&&!ay(f)?"ready"===f.type&&bs.overdue===!0?bd.emit({type:"error",name:"flash-overdue"}):(e=bM({},f),bL.call(this,e),"copy"===f.type&&(g=aW(bo),h=g.data,bm=g.formatMap),h):void 0
},b5=function(){var d=bs.sandboxed;
if(b2(),"boolean"!=typeof bs.ready&&(bs.ready=!1),bs.sandboxed!==d&&bs.sandboxed===!0){bs.ready=!1,bd.emit({type:"error",name:"flash-sandboxed"})
}else{if(!bd.isFlashUnusable()&&null===bs.bridge){var c=be.flashLoadTimeout;
"number"==typeof c&&c>=0&&(bl=aX(function(){"boolean"!=typeof bs.deactivated&&(bs.deactivated=!0),bs.deactivated===!0&&bd.emit({type:"error",name:"flash-deactivated"})
},c)),bs.overdue=!1,bU()
}}},bT=function(){bd.clearData(),bd.blur(),bd.emit("destroy"),by(),bd.off()
},bx=function(f,e){var h;
if("object"==typeof f&&f&&"undefined"==typeof e){h=f,bd.clearData()
}else{if("string"!=typeof f||!f){return
}h={},h[f]=e
}for(var g in h){"string"==typeof g&&g&&aE.call(h,g)&&"string"==typeof h[g]&&h[g]&&(bo[g]=h[g])
}},aV=function(b){"undefined"==typeof b?(bG(bo),bm=null):"string"==typeof b&&aE.call(bo,b)&&delete bo[b]
},au=function(b){return"undefined"==typeof b?bK(bo):"string"==typeof b&&aE.call(bo,b)?bo[b]:void 0
},aj=function(f){if(f&&1===f.nodeType){a4&&(am(a4,be.activeClass),a4!==f&&am(a4,be.hoverClass)),a4=f,ax(f,be.hoverClass);
var d=f.getAttribute("title")||be.title;
if("string"==typeof d&&d){var h=b6(bs.bridge);
h&&h.setAttribute("title",d)
}var g=be.forceHandCursor===!0||"pointer"===cd(f,"cursor");
ao(g),az()
}},b8=function(){var b=b6(bs.bridge);
b&&(b.removeAttribute("title"),b.style.left="0px",b.style.top="-9999px",b.style.width="1px",b.style.height="1px"),a4&&(am(a4,be.hoverClass),am(a4,be.activeClass),a4=null)
},bW=function(){return a4||null
},bE=function(b){return"string"==typeof b&&b&&/^[A-Za-z][A-Za-z0-9_:\-\.]*$/.test(b)
},a2=function(e){var d;
if("string"==typeof e&&e?(d=e,e={}):"object"==typeof e&&e&&"string"==typeof e.type&&e.type&&(d=e.type),d){d=d.toLowerCase(),!e.target&&(/^(copy|aftercopy|_click)$/.test(d)||"error"===d&&"clipboard-error"===e.name)&&(e.target=a1),bM(e,{type:d,target:e.target||a4||null,relatedTarget:e.relatedTarget||null,currentTarget:bs&&bs.bridge||null,timeStamp:e.timeStamp||aH()||null});
var f=bj[e.type];
return"error"===e.type&&e.name&&f&&(f=f[e.name]),f&&(e.message=f),"ready"===e.type&&bM(e,{target:null,version:bs.version}),"error"===e.type&&(bg.test(e.name)&&bM(e,{target:null,minimumVersion:br}),bf.test(e.name)&&bM(e,{version:bs.version})),"copy"===e.type&&(e.clipboardData={setData:bd.setData,clearData:bd.clearData}),"aftercopy"===e.type&&(e=av(e,bm)),e.target&&!e.relatedTarget&&(e.relatedTarget=aw(e.target)),al(e)
}},aw=function(d){var c=d&&d.getAttribute&&d.getAttribute("data-clipboard-target");
return c?aZ.getElementById(c):null
},al=function(F){if(F&&/^_(?:click|mouse(?:over|out|down|up|move))$/.test(F.type)){var E=F.target,D="_mouseover"===F.type&&F.relatedTarget?F.relatedTarget:a6,C="_mouseout"===F.type&&F.relatedTarget?F.relatedTarget:a6,B=b0(E),A=a0.screenLeft||a0.screenX||0,z=a0.screenTop||a0.screenY||0,y=aZ.body.scrollLeft+aZ.documentElement.scrollLeft,x=aZ.body.scrollTop+aZ.documentElement.scrollTop,w=B.left+("number"==typeof F._stageX?F._stageX:0),v=B.top+("number"==typeof F._stageY?F._stageY:0),u=w-y,g=v-x,f=A+u,b=z+g,H="number"==typeof F.movementX?F.movementX:0,G="number"==typeof F.movementY?F.movementY:0;
delete F._stageX,delete F._stageY,bM(F,{srcElement:E,fromElement:D,toElement:C,screenX:f,screenY:b,pageX:w,pageY:v,clientX:u,clientY:g,x:u,y:g,movementX:H,movementY:G,offsetX:0,offsetY:0,layerX:0,layerY:0})
}return F
},ca=function(d){var c=d&&"string"==typeof d.type&&d.type||"";
return !/^(?:(?:before)?copy|destroy)$/.test(c)
},bZ=function(f,e,h,g){g?aX(function(){f.apply(e,h)
},0):f.apply(e,h)
},bL=function(v){if("object"==typeof v&&v&&v.type){var u=ca(v),t=bq["*"]||[],s=bq[v.type]||[],r=t.concat(s);
if(r&&r.length){var q,p,o,n,m,f=this;
for(q=0,p=r.length;
p>q;
q++){o=r[q],n=f,"string"==typeof o&&"function"==typeof a0[o]&&(o=a0[o]),"object"==typeof o&&o&&"function"==typeof o.handleEvent&&(n=o,o=o.handleEvent),"function"==typeof o&&(m=bM({},v),bZ(o,n,[m],u))
}}return this
}},a8=function(d){var c=null;
return(bt===!1||d&&"error"===d.type&&d.name&&-1!==bi.indexOf(d.name))&&(c=!1),c
},ay=function(d){var c=d.target||a4||null,p="swf"===d._source;
switch(delete d._source,d.type){case"error":var o="flash-sandboxed"===d.name||a8(d);
"boolean"==typeof o&&(bs.sandboxed=o),-1!==bh.indexOf(d.name)?bM(bs,{disabled:"flash-disabled"===d.name,outdated:"flash-outdated"===d.name,unavailable:"flash-unavailable"===d.name,degraded:"flash-degraded"===d.name,deactivated:"flash-deactivated"===d.name,overdue:"flash-overdue"===d.name,ready:!1}):"version-mismatch"===d.name&&(a5=d.swfVersion,bM(bs,{disabled:!1,outdated:!1,unavailable:!1,degraded:!1,deactivated:!1,overdue:!1,ready:!1})),a9();
break;
case"ready":a5=d.swfVersion;
var n=bs.deactivated===!0;
bM(bs,{disabled:!1,outdated:!1,sandboxed:!1,unavailable:!1,degraded:!1,deactivated:!1,overdue:n,ready:!n}),a9();
break;
case"beforecopy":a1=c;
break;
case"copy":var m,l,e=d.relatedTarget;
!bo["text/html"]&&!bo["text/plain"]&&e&&(l=e.value||e.outerHTML||e.innerHTML)&&(m=e.value||e.textContent||e.innerText)?(d.clipboardData.clearData(),d.clipboardData.setData("text/plain",m),l!==m&&d.clipboardData.setData("text/html",l)):!bo["text/plain"]&&d.target&&(m=d.target.getAttribute("data-clipboard-text"))&&(d.clipboardData.clearData(),d.clipboardData.setData("text/plain",m));
break;
case"aftercopy":an(d),bd.clearData(),c&&c!==a3()&&c.focus&&c.focus();
break;
case"_mouseover":bd.focus(c),be.bubbleEvents===!0&&p&&(c&&c!==d.relatedTarget&&!bD(d.relatedTarget,c)&&aa(bM({},d,{type:"mouseenter",bubbles:!1,cancelable:!1})),aa(bM({},d,{type:"mouseover"})));
break;
case"_mouseout":bd.blur(),be.bubbleEvents===!0&&p&&(c&&c!==d.relatedTarget&&!bD(d.relatedTarget,c)&&aa(bM({},d,{type:"mouseleave",bubbles:!1,cancelable:!1})),aa(bM({},d,{type:"mouseout"})));
break;
case"_mousedown":ax(c,be.activeClass),be.bubbleEvents===!0&&p&&aa(bM({},d,{type:d.type.slice(1)}));
break;
case"_mouseup":am(c,be.activeClass),be.bubbleEvents===!0&&p&&aa(bM({},d,{type:d.type.slice(1)}));
break;
case"_click":a1=null,be.bubbleEvents===!0&&p&&aa(bM({},d,{type:d.type.slice(1)}));
break;
case"_mousemove":be.bubbleEvents===!0&&p&&aa(bM({},d,{type:d.type.slice(1)}))
}return/^_(?:click|mouse(?:over|out|down|up|move))$/.test(d.type)?!0:void 0
},an=function(d){if(d.errors&&d.errors.length>0){var c=bK(d);
bM(c,{type:"error",name:"clipboard-error"}),delete c.success,aX(function(){bd.emit(c)
},0)
}},aa=function(g){if(g&&"string"==typeof g.type&&g){var f,l=g.target||null,k=l&&l.ownerDocument||aZ,j={view:k.defaultView||a0,canBubble:!0,cancelable:!0,detail:"click"===g.type?1:0,button:"number"==typeof g.which?g.which-1:"number"==typeof g.button?g.button:k.createEvent?0:1},i=bM(j,g);
l&&k.createEvent&&l.dispatchEvent&&(i=[i.type,i.canBubble,i.cancelable,i.view,i.detail,i.screenX,i.screenY,i.clientX,i.clientY,i.ctrlKey,i.altKey,i.shiftKey,i.metaKey,i.button,i.relatedTarget],f=k.createEvent("MouseEvents"),f.initMouseEvent&&(f.initMouseEvent.apply(f,i),f._source="js",l.dispatchEvent(f)))
}},b1=function(){var e=be.flashLoadTimeout;
if("number"==typeof e&&e>=0){var d=Math.min(1000,e/10),f=be.swfObjectId+"_fallbackContent";
bk=aT(function(){var b=aZ.getElementById(f);
bN(b)&&(a9(),bs.deactivated=null,bd.emit({type:"error",name:"swf-not-found"}))
},d)
}},bO=function(){var b=aZ.createElement("div");
return b.id=be.containerId,b.className=be.containerClass,b.style.position="absolute",b.style.left="0px",b.style.top="-9999px",b.style.width="1px",b.style.height="1px",b.style.zIndex=""+ad(be.zIndex),b
},b6=function(d){for(var c=d&&d.parentNode;
c&&"OBJECT"===c.nodeName&&c.parentNode;
){c=c.parentNode
}return c||null
},bU=function(){var t,s=bs.bridge,r=b6(s);
if(!s){var q=bF(a0.location.host,be),p="never"===q?"none":"all",o=b9(bM({jsVersion:bd.version},be)),n=be.swfPath+ak(be.swfPath,be);
r=bO();
var m=aZ.createElement("div");
r.appendChild(m),aZ.body.appendChild(r);
var g=aZ.createElement("div"),f="activex"===bs.pluginType;
g.innerHTML='<object id="'+be.swfObjectId+'" name="'+be.swfObjectId+'" width="100%" height="100%" '+(f?'classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"':'type="application/x-shockwave-flash" data="'+n+'"')+">"+(f?'<param name="movie" value="'+n+'"/>':"")+'<param name="allowScriptAccess" value="'+q+'"/><param name="allowNetworking" value="'+p+'"/><param name="menu" value="false"/><param name="wmode" value="transparent"/><param name="flashvars" value="'+o+'"/><div id="'+be.swfObjectId+'_fallbackContent">&nbsp;</div></object>',s=g.firstChild,g=null,aC(s).ZeroClipboard=bd,r.replaceChild(s,m),b1()
}return s||(s=aZ[be.swfObjectId],s&&(t=s.length)&&(s=s[t-1]),!s&&r&&(s=r.firstChild)),bs.bridge=s||null,s
},by=function(){var b=bs.bridge;
if(b){var f=b6(b);
f&&("activex"===bs.pluginType&&"readyState" in b?(b.style.display="none",function c(){if(4===b.readyState){for(var a in b){"function"==typeof b[a]&&(b[a]=null)
}b.parentNode&&b.parentNode.removeChild(b),f.parentNode&&f.parentNode.removeChild(f)
}else{aX(c,10)
}}()):(b.parentNode&&b.parentNode.removeChild(b),f.parentNode&&f.parentNode.removeChild(f))),a9(),bs.ready=null,bs.bridge=null,bs.deactivated=null,a5=a6
}},aW=function(f){var e={},h={};
if("object"==typeof f&&f){for(var g in f){if(g&&aE.call(f,g)&&"string"==typeof f[g]&&f[g]){switch(g.toLowerCase()){case"text/plain":case"text":case"air:text":case"flash:text":e.text=f[g],h.text=g;
break;
case"text/html":case"html":case"air:html":case"flash:html":e.html=f[g],h.html=g;
break;
case"application/rtf":case"text/rtf":case"rtf":case"richtext":case"air:rtf":case"flash:rtf":e.rtf=f[g],h.rtf=g
}}}return{data:e,formatMap:h}
}},av=function(j,i){if("object"!=typeof j||!j||"object"!=typeof i||!i){return j
}var p={};
for(var o in j){if(aE.call(j,o)){if("errors"===o){p[o]=j[o]?j[o].slice():[];
for(var n=0,m=p[o].length;
m>n;
n++){p[o][n].format=i[p[o][n].format]
}}else{if("success"!==o&&"data"!==o){p[o]=j[o]
}else{p[o]={};
var l=j[o];
for(var k in l){k&&aE.call(l,k)&&aE.call(i,k)&&(p[o][i[k]]=l[k])
}}}}}return p
},ak=function(e,d){var f=null==d||d&&d.cacheBust===!0;
return f?(-1===e.indexOf("?")?"?":"&")+"noCache="+aH():""
},b9=function(i){var f,n,m,l,k="",j=[];
if(i.trustedDomains&&("string"==typeof i.trustedDomains?l=[i.trustedDomains]:"object"==typeof i.trustedDomains&&"length" in i.trustedDomains&&(l=i.trustedDomains)),l&&l.length){for(f=0,n=l.length;
n>f;
f++){if(aE.call(l,f)&&l[f]&&"string"==typeof l[f]){if(m=bX(l[f]),!m){continue
}if("*"===m){j.length=0,j.push(m);
break
}j.push.apply(j,[m,"//"+m,a0.location.protocol+"//"+m])
}}}return j.length&&(k+="trustedOrigins="+aP(j.join(","))),i.forceEnhancedClipboard===!0&&(k+=(k?"&":"")+"forceEnhancedClipboard=true"),"string"==typeof i.swfObjectId&&i.swfObjectId&&(k+=(k?"&":"")+"swfObjectId="+aP(i.swfObjectId)),"string"==typeof i.jsVersion&&i.jsVersion&&(k+=(k?"&":"")+"jsVersion="+aP(i.jsVersion)),k
},bX=function(e){if(null==e||""===e){return null
}if(e=e.replace(/^\s+|\s+$/g,""),""===e){return null
}var d=e.indexOf("//");
e=-1===d?e:e.slice(d+2);
var f=e.indexOf("/");
return e=-1===f?e:-1===d||0===f?null:e.slice(0,f),e&&".swf"===e.slice(-4).toLowerCase()?null:e||null
},bF=function(){var b=function(g){var f,j,i,h=[];
if("string"==typeof g&&(g=[g]),"object"!=typeof g||!g||"number"!=typeof g.length){return h
}for(f=0,j=g.length;
j>f;
f++){if(aE.call(g,f)&&(i=bX(g[f]))){if("*"===i){h.length=0,h.push("*");
break
}-1===h.indexOf(i)&&h.push(i)
}}return h
};
return function(a,j){var i=bX(j.swfPath);
null===i&&(i=a);
var h=b(j.trustedDomains),g=h.length;
if(g>0){if(1===g&&"*"===h[0]){return"always"
}if(-1!==h.indexOf(a)){return 1===g&&a===i?"sameDomain":"always"
}}return"never"
}
}(),a3=function(){try{return aZ.activeElement
}catch(b){return null
}},ax=function(h,g){var l,k,j,i=[];
if("string"==typeof g&&g&&(i=g.split(/\s+/)),h&&1===h.nodeType&&i.length>0){if(h.classList){for(l=0,k=i.length;
k>l;
l++){h.classList.add(i[l])
}}else{if(h.hasOwnProperty("className")){for(j=" "+h.className+" ",l=0,k=i.length;
k>l;
l++){-1===j.indexOf(" "+i[l]+" ")&&(j+=i[l]+" ")
}h.className=j.replace(/^\s+|\s+$/g,"")
}}}return h
},am=function(h,g){var l,k,j,i=[];
if("string"==typeof g&&g&&(i=g.split(/\s+/)),h&&1===h.nodeType&&i.length>0){if(h.classList&&h.classList.length>0){for(l=0,k=i.length;
k>l;
l++){h.classList.remove(i[l])
}}else{if(h.className){for(j=(" "+h.className+" ").replace(/[\r\n\t]/g," "),l=0,k=i.length;
k>l;
l++){j=j.replace(" "+i[l]+" "," ")
}h.className=j.replace(/^\s+|\s+$/g,"")
}}}return h
},cd=function(e,d){var f=aQ(e,null).getPropertyValue(d);
return"cursor"!==d||f&&"auto"!==f||"A"!==e.nodeName?f:"pointer"
},b0=function(v){var u={left:0,top:0,width:0,height:0};
if(v.getBoundingClientRect){var t=v.getBoundingClientRect(),s=a0.pageXOffset,r=a0.pageYOffset,q=aZ.documentElement.clientLeft||0,p=aZ.documentElement.clientTop||0,o=0,n=0;
if("relative"===cd(aZ.body,"position")){var g=aZ.body.getBoundingClientRect(),f=aZ.documentElement.getBoundingClientRect();
o=g.left-f.left||0,n=g.top-f.top||0
}u.left=t.left+s-q-o,u.top=t.top+r-p-n,u.width="width" in t?t.width:t.right-t.left,u.height="height" in t?t.height:t.bottom-t.top
}return u
},bN=function(r){if(!r){return !1
}var q=aQ(r,null),p=aJ(q.height)>0,o=aJ(q.width)>0,n=aJ(q.top)>=0,m=aJ(q.left)>=0,l=p&&o&&n&&m,k=l?null:b0(r),j="none"!==q.display&&"collapse"!==q.visibility&&(l||!!k&&(p||k.height>0)&&(o||k.width>0)&&(n||k.top>=0)&&(m||k.left>=0));
return j
},a9=function(){aU(bl),bl=0,aR(bk),bk=0
},az=function(){var d;
if(a4&&(d=b6(bs.bridge))){var c=b0(a4);
bM(d.style,{width:c.width+"px",height:c.height+"px",top:c.top+"px",left:c.left+"px",zIndex:""+ad(be.zIndex)})
}},ao=function(b){bs.ready===!0&&(bs.bridge&&"function"==typeof bs.bridge.setHandCursor?bs.bridge.setHandCursor(b):bs.ready=!1)
},ad=function(d){if(/^(?:auto|inherit)$/.test(d)){return d
}var c;
return"number"!=typeof d||aI(d)?"string"==typeof d&&(c=ad(aK(d,10))):c=d,"number"==typeof c?c:"auto"
},b2=function(a){var n,m,l,k=bs.sandboxed,j=null;
if(a=a===!0,bt===!1){j=!1
}else{try{m=a7.frameElement||null
}catch(i){l={name:i.name,message:i.message}
}if(m&&1===m.nodeType&&"IFRAME"===m.nodeName){try{j=m.hasAttribute("sandbox")
}catch(i){j=null
}}else{try{n=document.domain||null
}catch(i){n=null
}(null===n||l&&"SecurityError"===l.name&&/(^|[\s\(\[@])sandbox(es|ed|ing|[\s\.,!\)\]@]|$)/.test(l.message.toLowerCase()))&&(j=!0)
}}return bs.sandboxed=j,k===j||a||bP(aN),j
},bP=function(B){function A(d){var c=d.match(/[\d]+/g);
return c.length=3,c.join(".")
}function z(b){return !!b&&(b=b.toLowerCase())&&(/^(pepflashplayer\.dll|libpepflashplayer\.so|pepperflashplayer\.plugin)$/.test(b)||"chrome.plugin"===b.slice(-13))
}function y(b){b&&(u=!0,b.version&&(r=A(b.version)),!r&&b.description&&(r=A(b.description)),b.filename&&(s=z(b.filename)))
}var x,w,v,u=!1,t=!1,s=!1,r="";
if(aY.plugins&&aY.plugins.length){x=aY.plugins["Shockwave Flash"],y(x),aY.plugins["Shockwave Flash 2.0"]&&(u=!0,r="2.0.0.11")
}else{if(aY.mimeTypes&&aY.mimeTypes.length){v=aY.mimeTypes["application/x-shockwave-flash"],x=v&&v.enabledPlugin,y(x)
}else{if("undefined"!=typeof B){t=!0;
try{w=new B("ShockwaveFlash.ShockwaveFlash.7"),u=!0,r=A(w.GetVariable("$version"))
}catch(q){try{w=new B("ShockwaveFlash.ShockwaveFlash.6"),u=!0,r="6.0.21"
}catch(p){try{w=new B("ShockwaveFlash.ShockwaveFlash"),u=!0,r=A(w.GetVariable("$version"))
}catch(h){t=!1
}}}}}}bs.disabled=u!==!0,bs.outdated=r&&aJ(r)<aJ(br),bs.version=r||"0.0.0",bs.pluginType=s?"pepper":t?"activex":u?"netscape":"unknown"
};
bP(aN),b2(!0);
var bd=function(){return this instanceof bd?void ("function"==typeof bd._createClient&&bd._createClient.apply(this,aA(arguments))):new bd
};
aF(bd,"version",{value:"2.2.0",writable:!1,configurable:!0,enumerable:!0}),bd.config=function(){return bY.apply(this,aA(arguments))
},bd.state=function(){return ba.apply(this,aA(arguments))
},bd.isFlashUnusable=function(){return bR.apply(this,aA(arguments))
},bd.on=function(){return bp.apply(this,aA(arguments))
},bd.off=function(){return aO.apply(this,aA(arguments))
},bd.handlers=function(){return ar.apply(this,aA(arguments))
},bd.emit=function(){return ah.apply(this,aA(arguments))
},bd.create=function(){return b5.apply(this,aA(arguments))
},bd.destroy=function(){return bT.apply(this,aA(arguments))
},bd.setData=function(){return bx.apply(this,aA(arguments))
},bd.clearData=function(){return aV.apply(this,aA(arguments))
},bd.getData=function(){return au.apply(this,aA(arguments))
},bd.focus=bd.activate=function(){return aj.apply(this,aA(arguments))
},bd.blur=bd.deactivate=function(){return b8.apply(this,aA(arguments))
},bd.activeElement=function(){return bW.apply(this,aA(arguments))
};
var aB=0,ap={},ae=0,b3={},bH={};
bM(be,{autoActivate:!0});
var af=function(d){var c=this;
c.id=""+aB++,ap[c.id]={instance:c,elements:[],handlers:{}},d&&c.clip(d),bd.on("*",function(b){return c.emit(b)
}),bd.on("destroy",function(){c.destroy()
}),bd.create()
},bQ=function(b,p){var o,n,m,l={},k=ap[this.id],c=k&&k.handlers;
if(!k){throw new Error("Attempted to add new listener(s) to a destroyed ZeroClipboard client instance")
}if("string"==typeof b&&b){m=b.toLowerCase().split(/\s+/)
}else{if("object"==typeof b&&b&&"undefined"==typeof p){for(o in b){aE.call(b,o)&&"string"==typeof o&&o&&"function"==typeof b[o]&&this.on(o,b[o])
}}}if(m&&m.length){for(o=0,n=m.length;
n>o;
o++){b=m[o].replace(/^on/,""),l[b]=!0,c[b]||(c[b]=[]),c[b].push(p)
}if(l.ready&&bs.ready&&this.emit({type:"ready",client:this}),l.error){for(o=0,n=bh.length;
n>o;
o++){if(bs[bh[o].replace(/^flash-/,"")]){this.emit({type:"error",name:bh[o],client:this});
break
}}a5!==a6&&bd.version!==a5&&this.emit({type:"error",name:"version-mismatch",jsVersion:bd.version,swfVersion:a5})
}}return this
},bn=function(r,q){var p,o,n,m,l,k=ap[this.id],j=k&&k.handlers;
if(!j){return this
}if(0===arguments.length){m=aG(j)
}else{if("string"==typeof r&&r){m=r.split(/\s+/)
}else{if("object"==typeof r&&r&&"undefined"==typeof q){for(p in r){aE.call(r,p)&&"string"==typeof p&&p&&"function"==typeof r[p]&&this.off(p,r[p])
}}}}if(m&&m.length){for(p=0,o=m.length;
o>p;
p++){if(r=m[p].toLowerCase().replace(/^on/,""),l=j[r],l&&l.length){if(q){for(n=l.indexOf(q);
-1!==n;
){l.splice(n,1),n=l.indexOf(q,n)
}}else{l.length=0
}}}}return this
},aL=function(e){var d=null,f=ap[this.id]&&ap[this.id].handlers;
return f&&(d="string"==typeof e&&e?f[e]?f[e].slice(0):[]:bK(f)),d
},aq=function(d){if(aS.call(this,d)){"object"==typeof d&&d&&"string"==typeof d.type&&d.type&&(d=bM({},d));
var c=bM({},a2(d),{client:this});
at.call(this,c)
}return this
},ag=function(e){if(!ap[this.id]){throw new Error("Attempted to clip element(s) to a destroyed ZeroClipboard client instance")
}e=ai(e);
for(var d=0;
d<e.length;
d++){if(aE.call(e,d)&&e[d]&&1===e[d].nodeType){e[d].zcClippingId?-1===b3[e[d].zcClippingId].indexOf(this.id)&&b3[e[d].zcClippingId].push(this.id):(e[d].zcClippingId="zcClippingId_"+ae++,b3[e[d].zcClippingId]=[this.id],be.autoActivate===!0&&b7(e[d]));
var f=ap[this.id]&&ap[this.id].elements;
-1===f.indexOf(e[d])&&f.push(e[d])
}}return this
},b4=function(h){var g=ap[this.id];
if(!g){return this
}var l,k=g.elements;
h="undefined"==typeof h?k.slice(0):ai(h);
for(var j=h.length;
j--;
){if(aE.call(h,j)&&h[j]&&1===h[j].nodeType){for(l=0;
-1!==(l=k.indexOf(h[j],l));
){k.splice(l,1)
}var i=b3[h[j].zcClippingId];
if(i){for(l=0;
-1!==(l=i.indexOf(this.id,l));
){i.splice(l,1)
}0===i.length&&(be.autoActivate===!0&&bV(h[j]),delete h[j].zcClippingId)
}}}return this
},bS=function(){var b=ap[this.id];
return b&&b.elements?b.elements.slice(0):[]
},bu=function(){ap[this.id]&&(this.unclip(),this.off(),delete ap[this.id])
},aS=function(i){if(!i||!i.type){return !1
}if(i.client&&i.client!==this){return !1
}var h=ap[this.id],n=h&&h.elements,m=!!n&&n.length>0,l=!i.target||m&&-1!==n.indexOf(i.target),k=i.relatedTarget&&m&&-1!==n.indexOf(i.relatedTarget),j=i.client&&i.client===this;
return h&&(l||k||j)?!0:!1
},at=function(x){var w=ap[this.id];
if("object"==typeof x&&x&&x.type&&w){var v=ca(x),u=w&&w.handlers["*"]||[],t=w&&w.handlers[x.type]||[],s=u.concat(t);
if(s&&s.length){var r,q,p,o,n,f=this;
for(r=0,q=s.length;
q>r;
r++){p=s[r],o=f,"string"==typeof p&&"function"==typeof a0[p]&&(p=a0[p]),"object"==typeof p&&p&&"function"==typeof p.handleEvent&&(o=p,p=p.handleEvent),"function"==typeof p&&(n=bM({},x),bZ(p,o,[n],v))
}}}},ai=function(b){return"string"==typeof b&&(b=[]),"number"!=typeof b.length?[b]:b
},b7=function(e){if(e&&1===e.nodeType){var d=function(b){(b||(b=a0.event))&&("js"!==b._source&&(b.stopImmediatePropagation(),b.preventDefault()),delete b._source)
},f=function(a){(a||(a=a0.event))&&(d(a),bd.focus(e))
};
e.addEventListener("mouseover",f,!1),e.addEventListener("mouseout",d,!1),e.addEventListener("mouseenter",d,!1),e.addEventListener("mouseleave",d,!1),e.addEventListener("mousemove",d,!1),bH[e.zcClippingId]={mouseover:f,mouseout:d,mouseenter:d,mouseleave:d,mousemove:d}
}},bV=function(i){if(i&&1===i.nodeType){var h=bH[i.zcClippingId];
if("object"==typeof h&&h){for(var n,m,l=["move","leave","enter","out","over"],k=0,j=l.length;
j>k;
k++){n="mouse"+l[k],m=h[n],"function"==typeof m&&i.removeEventListener(n,m,!1)
}delete bH[i.zcClippingId]
}}};
bd._createClient=function(){af.apply(this,aA(arguments))
},bd.prototype.on=function(){return bQ.apply(this,aA(arguments))
},bd.prototype.off=function(){return bn.apply(this,aA(arguments))
},bd.prototype.handlers=function(){return aL.apply(this,aA(arguments))
},bd.prototype.emit=function(){return aq.apply(this,aA(arguments))
},bd.prototype.clip=function(){return ag.apply(this,aA(arguments))
},bd.prototype.unclip=function(){return b4.apply(this,aA(arguments))
},bd.prototype.elements=function(){return bS.apply(this,aA(arguments))
},bd.prototype.destroy=function(){return bu.apply(this,aA(arguments))
},bd.prototype.setText=function(b){if(!ap[this.id]){throw new Error("Attempted to set pending clipboard data from a destroyed ZeroClipboard client instance")
}return bd.setData("text/plain",b),this
},bd.prototype.setHtml=function(b){if(!ap[this.id]){throw new Error("Attempted to set pending clipboard data from a destroyed ZeroClipboard client instance")
}return bd.setData("text/html",b),this
},bd.prototype.setRichText=function(b){if(!ap[this.id]){throw new Error("Attempted to set pending clipboard data from a destroyed ZeroClipboard client instance")
}return bd.setData("application/rtf",b),this
},bd.prototype.setData=function(){if(!ap[this.id]){throw new Error("Attempted to set pending clipboard data from a destroyed ZeroClipboard client instance")
}return bd.setData.apply(this,aA(arguments)),this
},bd.prototype.clearData=function(){if(!ap[this.id]){throw new Error("Attempted to clear pending clipboard data from a destroyed ZeroClipboard client instance")
}return bd.clearData.apply(this,aA(arguments)),this
},bd.prototype.getData=function(){if(!ap[this.id]){throw new Error("Attempted to get pending clipboard data from a destroyed ZeroClipboard client instance")
}return bd.getData.apply(this,aA(arguments))
},"function"==typeof define&&define.amd?define(function(){return bd
}):"object"==typeof module&&module&&"object"==typeof module.exports&&module.exports?module.exports=bd:a7.ZeroClipboard=bd
}(function(){return this||window
}());
/*! fluidvids.js v2.4.1 | (c) 2014 @toddmotto | https://github.com/toddmotto/fluidvids */
;
!function(b,a){"function"==typeof define&&define.amd?define(a):"object"==typeof exports?module.exports=a:b.fluidvids=a()
}(this,function(){function f(d){return new RegExp("^(https?:)?//(?:"+h.players.join("|")+").*$","i").test(d)
}function b(i,d){return parseInt(i,10)/parseInt(d,10)*100+"%"
}function a(d){if((f(d.src)||f(d.data))&&!d.getAttribute("data-fluidvids")){var e=document.createElement("div");
d.parentNode.insertBefore(e,d),d.className+=(d.className?" ":"")+"fluidvids-item",d.setAttribute("data-fluidvids","loaded"),e.className+="fluidvids",e.style.paddingTop=b(d.height,d.width),e.appendChild(d)
}}function j(){var d=document.createElement("div");
d.innerHTML="<p>x</p><style>"+g+"</style>",c.appendChild(d.childNodes[1])
}var h={selector:["iframe","object"],players:["www.youtube.com","player.vimeo.com"]},g=[".fluidvids {","width: 100%; max-width: 100%; position: relative;","}",".fluidvids-item {","position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;","}"].join(""),c=document.head||document.getElementsByTagName("head")[0];
return h.render=function(){for(var i=document.querySelectorAll(h.selector.join()),d=i.length;
d--;
){a(i[d])
}},h.init=function(i){for(var d in i){h[d]=i[d]
}h.render(),j()
},h
});
/*! Picturefill - v3.0.1 - 2015-09-30
 * http://scottjehl.github.io/picturefill
 * Copyright (c) 2015 https://github.com/scottjehl/picturefill/blob/master/Authors.txt; Licensed MIT
 */
;
!function(d){var c=navigator.userAgent;
d.HTMLPictureElement&&/ecko/.test(c)&&c.match(/rv\:(\d+)/)&&RegExp.$1<41&&addEventListener("resize",function(){var a,n=document.createElement("source"),m=function(g){var f,o,h=g.parentNode;
"PICTURE"===h.nodeName.toUpperCase()?(f=n.cloneNode(),h.insertBefore(f,h.firstElementChild),setTimeout(function(){h.removeChild(f)
})):(!g._pfLastSize||g.offsetWidth>g._pfLastSize)&&(g._pfLastSize=g.offsetWidth,o=g.sizes,g.sizes+=",100vw",setTimeout(function(){g.sizes=o
}))
},l=function(){var f,e=document.querySelectorAll("picture > img, img[srcset][sizes]");
for(f=0;
f<e.length;
f++){m(e[f])
}},k=function(){clearTimeout(a),a=setTimeout(l,99)
},j=d.matchMedia&&matchMedia("(orientation: landscape)"),i=function(){k(),j&&j.addListener&&j.addListener(k)
};
return n.srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==",/^[c|i]|d$/.test(document.readyState||"")?i():document.addEventListener("DOMContentLoaded",i),k
}())
}(window),function(a3,a2,a1){function a0(b){return" "===b||"  "===b||"\n"===b||"\f"===b||"\r"===b
}function aZ(a,f){var e=new a3.Image;
return e.onerror=function(){aE[a]=!1,aC()
},e.onload=function(){aE[a]=1===e.width,aC()
},e.src=f,"pending"
}function aY(){ap=!1,am=a3.devicePixelRatio,ao={},an={},aL.DPR=am||1,al.width=Math.max(a3.innerWidth||0,aF.clientWidth),al.height=Math.max(a3.innerHeight||0,aF.clientHeight),al.vw=al.width/100,al.vh=al.height/100,aM=[al.height,al.width,am].join("-"),al.em=aL.getEmValue(),al.rem=al.em
}function aX(j,i,p,o){var n,m,l,k;
return"saveData"===aB.algorithm?j>2.7?k=p+1:(m=i-p,n=Math.pow(j-0.6,1.5),l=m*n,o&&(l+=0.1*n),k=j+l):k=p>1?Math.sqrt(j*i):j,k>p
}function aW(f){var e,h=aL.getSet(f),g=!1;
"pending"!==h&&(g=aM,h&&(e=aL.setRes(h),aL.applySetCandidate(e,f))),f[aL.ns].evaled=g
}function aV(d,c){return d.res-c.res
}function aU(f,e,h){var g;
return !h&&e&&(h=f[aL.ns].sets,h=h&&h[h.length-1]),g=aT(e,h),g&&(e=aL.makeUrl(e),f[aL.ns].curSrc=e,f[aL.ns].curCan=g,g.res||a4(g,g.set.sizes)),g
}function aT(g,f){var j,i,h;
if(g&&f){for(h=aL.parseSet(f),g=aL.makeUrl(g),j=0;
j<h.length;
j++){if(g===aL.makeUrl(h[j].url)){i=h[j];
break
}}}return i
}function aS(i,h){var n,m,l,k,j=i.getElementsByTagName("source");
for(n=0,m=j.length;
m>n;
n++){l=j[n],l[aL.ns]=!0,k=l.getAttribute("srcset"),k&&h.push({srcset:k,media:l.getAttribute("media"),type:l.getAttribute("type"),sizes:l.getAttribute("sizes")})
}}function aR(z,y){function x(a){var f,e=a.exec(z.substring(o));
return e?(f=e[0],o+=f.length,f):void 0
}function w(){var G,F,E,D,C,B,A,n,h,g=!1,b={};
for(D=0;
D<t.length;
D++){C=t[D],B=C[C.length-1],A=C.substring(0,C.length-1),n=parseInt(A,10),h=parseFloat(A),ae.test(A)&&"w"===B?((G||F)&&(g=!0),0===n?g=!0:G=n):ad.test(A)&&"x"===B?((G||F||E)&&(g=!0),0>h?g=!0:F=h):ae.test(A)&&"h"===B?((E||F)&&(g=!0),0===n?g=!0:E=n):g=!0
}g||(b.url=u,G&&(b.w=G),F&&(b.d=F),E&&(b.h=E),E||F||G||(b.d=1),1===b.d&&(y.has1x=!0),b.set=y,d.push(b))
}function v(){for(x(ai),s="",r="in descriptor";
;
){if(q=z.charAt(o),"in descriptor"===r){if(a0(q)){s&&(t.push(s),s="",r="after descriptor")
}else{if(","===q){return o+=1,s&&t.push(s),void w()
}if("("===q){s+=q,r="in parens"
}else{if(""===q){return s&&t.push(s),void w()
}s+=q
}}}else{if("in parens"===r){if(")"===q){s+=q,r="in descriptor"
}else{if(""===q){return t.push(s),void w()
}s+=q
}}else{if("after descriptor"===r){if(a0(q)){}else{if(""===q){return void w()
}r="in descriptor",o-=1
}}}}o+=1
}}for(var u,t,s,r,q,p=z.length,o=0,d=[];
;
){if(x(ah),o>=p){return d
}u=x(ag),t=[],","===u.slice(-1)?(u=u.replace(af,""),w()):v()
}}function aQ(v){function u(E){function D(){A&&(z.push(A),A="")
}function C(){z[0]&&(y.push(z),z=[])
}for(var B,A="",z=[],y=[],x=0,w=0,l=!1;
;
){if(B=E.charAt(w),""===B){return D(),C(),y
}if(l){if("*"===B&&"/"===E[w+1]){l=!1,w+=2,D();
continue
}w+=1
}else{if(a0(B)){if(E.charAt(w-1)&&a0(E.charAt(w-1))||!A){w+=1;
continue
}if(0===x){D(),w+=1;
continue
}B=" "
}else{if("("===B){x+=1
}else{if(")"===B){x-=1
}else{if(","===B){D(),C(),w+=1;
continue
}if("/"===B&&"*"===E.charAt(w+1)){l=!0,w+=2;
continue
}}}}A+=B,w+=1
}}}function t(b){return m.test(b)&&parseFloat(b)>=0?!0:d.test(b)?!0:"0"===b||"-0"===b||"+0"===b?!0:!1
}var s,r,q,p,o,n,m=/^(?:[+-]?[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?(?:ch|cm|em|ex|in|mm|pc|pt|px|rem|vh|vmin|vmax|vw)$/i,d=/^calc\((?:[0-9a-z \.\+\-\*\/\(\)]+)\)$/i;
for(r=u(v),q=r.length,s=0;
q>s;
s++){if(p=r[s],o=p[p.length-1],t(o)){if(n=o,p.pop(),0===p.length){return n
}if(p=p.join(" "),aL.matchesMedia(p)){return n
}}}return"100vw"
}a2.createElement("picture");
var aP,aO,aN,aM,aL={},aK=function(){},aJ=a2.createElement("img"),aI=aJ.getAttribute,aH=aJ.setAttribute,aG=aJ.removeAttribute,aF=a2.documentElement,aE={},aB={algorithm:""},aA="data-pfsrc",az=aA+"set",ay=navigator.userAgent,ax=/rident/.test(ay)||/ecko/.test(ay)&&ay.match(/rv\:(\d+)/)&&RegExp.$1>35,aw="currentSrc",av=/\s+\+?\d+(e\d+)?w/,au=/(\([^)]+\))?\s*(.+)/,at=a3.picturefillCFG,ar="position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)",aq="font-size:100%!important;",ap=!0,ao={},an={},am=a3.devicePixelRatio,al={px:1,"in":96},ak=a2.createElement("a"),aj=!1,ai=/^[ \t\n\r\u000c]+/,ah=/^[, \t\n\r\u000c]+/,ag=/^[^ \t\n\r\u000c]+/,af=/[,]+$/,ae=/^\d+$/,ad=/^-?(?:[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/,ac=function(f,e,h,g){f.addEventListener?f.addEventListener(e,h,g||!1):f.attachEvent&&f.attachEvent("on"+e,h)
},ab=function(d){var c={};
return function(a){return a in c||(c[a]=d(a)),c[a]
}
},aD=function(){var e=/^([\d\.]+)(em|vw|px)$/,d=function(){for(var h=arguments,g=0,i=h[0];
++g in h;
){i=i.replace(h[g],h[++g])
}return i
},f=ab(function(b){return"return "+d((b||"").toLowerCase(),/\band\b/g,"&&",/,/g,"||",/min-([a-z-\s]+):/g,"e.$1>=",/max-([a-z-\s]+):/g,"e.$1<=",/calc([^)]+)/g,"($1)",/(\d+[\.]*[\d]*)([a-z]+)/g,"($1 * e.$2)",/^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/gi,"")+";"
});
return function(a,h){var g;
if(!(a in ao)){if(ao[a]=!1,h&&(g=a.match(e))){ao[a]=g[1]*al[g[2]]
}else{try{ao[a]=new Function("e",f(a))(al)
}catch(c){}}}return ao[a]
}
}(),a4=function(d,c){return d.w?(d.cWidth=aL.calcListLength(c||"100vw"),d.res=d.w/d.cWidth):d.res=d.d,d
},aC=function(b){var j,i,h,g=b||{};
if(g.elements&&1===g.elements.nodeType&&("IMG"===g.elements.nodeName.toUpperCase()?g.elements=[g.elements]:(g.context=g.elements,g.elements=null)),j=g.elements||aL.qsa(g.context||a2,g.reevaluate||g.reselect?aL.sel:aL.selShort),h=j.length){for(aL.setupRun(g),aj=!0,i=0;
h>i;
i++){aL.fillImg(j[i],g)
}aL.teardownRun(g)
}};
aP=a3.console&&console.warn?function(b){console.warn(b)
}:aK,aw in aJ||(aw="src"),aE["image/jpeg"]=!0,aE["image/gif"]=!0,aE["image/png"]=!0,aE["image/svg+xml"]=a2.implementation.hasFeature("http://wwwindow.w3.org/TR/SVG11/feature#Image","1.1"),aL.ns=("pf"+(new Date).getTime()).substr(0,9),aL.supSrcset="srcset" in aJ,aL.supSizes="sizes" in aJ,aL.supPicture=!!a3.HTMLPictureElement,aL.supSrcset&&aL.supPicture&&!aL.supSizes&&!function(b){aJ.srcset="data:,a",b.src="data:,a",aL.supSrcset=aJ.complete===b.complete,aL.supPicture=aL.supSrcset&&aL.supPicture
}(a2.createElement("img")),aL.selShort="picture>img,img[srcset]",aL.sel=aL.selShort,aL.cfg=aB,aL.supSrcset&&(aL.sel+=",img["+az+"]"),aL.DPR=am||1,aL.u=al,aL.types=aE,aN=aL.supSrcset&&!aL.supSizes,aL.setSize=aK,aL.makeUrl=ab(function(b){return ak.href=b,ak.href
}),aL.qsa=function(d,c){return d.querySelectorAll(c)
},aL.matchesMedia=function(){return a3.matchMedia&&(matchMedia("(min-width: 0.1em)")||{}).matches?aL.matchesMedia=function(b){return !b||matchMedia(b).matches
}:aL.matchesMedia=aL.mMQ,aL.matchesMedia.apply(this,arguments)
},aL.mMQ=function(b){return b?aD(b):!0
},aL.calcLength=function(d){var c=aD(d,!0)||!1;
return 0>c&&(c=!1),c
},aL.supportsType=function(b){return b?aE[b]:!0
},aL.parseSize=ab(function(d){var c=(d||"").match(au);
return{media:c&&c[1],length:c&&c[2]}
}),aL.parseSet=function(b){return b.cands||(b.cands=aR(b.srcset,b)),b.cands
},aL.getEmValue=function(){var b;
if(!aO&&(b=a2.body)){var h=a2.createElement("div"),g=aF.style.cssText,f=b.style.cssText;
h.style.cssText=ar,aF.style.cssText=aq,b.style.cssText=aq,b.appendChild(h),aO=h.offsetWidth,b.removeChild(h),aO=parseFloat(aO,10),aF.style.cssText=g,b.style.cssText=f
}return aO||16
},aL.calcListLength=function(d){if(!(d in an)||aB.uT){var c=aL.calcLength(aQ(d));
an[d]=c?c:al.width
}return an[d]
},aL.setRes=function(f){var e;
if(f){e=aL.parseSet(f);
for(var h=0,g=e.length;
g>h;
h++){a4(e[h],f.sizes)
}}return e
},aL.setRes.res=a4,aL.applySetCandidate=function(z,y){if(z.length){var x,w,v,u,t,s,r,q,j,i=y[aL.ns],g=aL.DPR;
if(s=i.curSrc||y[aw],r=i.curCan||aU(y,s,z[0].set),r&&r.set===z[0].set&&(j=ax&&!y.complete&&r.res-0.1>g,j||(r.cached=!0,r.res>=g&&(t=r))),!t){for(z.sort(aV),u=z.length,t=z[u-1],w=0;
u>w;
w++){if(x=z[w],x.res>=g){v=w-1,t=z[v]&&(j||s!==aL.makeUrl(x.url))&&aX(z[v].res,x.res,g,z[v].cached)?z[v]:x;
break
}}}t&&(q=aL.makeUrl(t.url),i.curSrc=q,i.curCan=t,q!==s&&aL.setSrc(y,t),aL.setSize(y))
}},aL.setSrc=function(e,d){var f;
e.src=d.url,"image/svg+xml"===d.set.type&&(f=e.style.width,e.style.width=e.offsetWidth+1+"px",e.offsetWidth+1&&(e.style.width=f))
},aL.getSet=function(h){var g,l,k,j=!1,i=h[aL.ns].sets;
for(g=0;
g<i.length&&!j;
g++){if(l=i[g],l.srcset&&aL.matchesMedia(l.media)&&(k=aL.supportsType(l.type))){"pending"===k&&(l=k),j=l;
break
}}return j
},aL.parseSets=function(r,q,p){var o,n,m,l,k=q&&"PICTURE"===q.nodeName.toUpperCase(),c=r[aL.ns];
(c.src===a1||p.src)&&(c.src=aI.call(r,"src"),c.src?aH.call(r,aA,c.src):aG.call(r,aA)),(c.srcset===a1||p.srcset||!aL.supSrcset||r.srcset)&&(o=aI.call(r,"srcset"),c.srcset=o,l=!0),c.sets=[],k&&(c.pic=!0,aS(q,c.sets)),c.srcset?(n={srcset:c.srcset,sizes:aI.call(r,"sizes")},c.sets.push(n),m=(aN||c.src)&&av.test(c.srcset||""),m||!c.src||aT(c.src,n)||n.has1x||(n.srcset+=", "+c.src,n.cands.push({url:c.src,d:1,set:n}))):c.src&&c.sets.push({srcset:c.src,sizes:null}),c.curCan=null,c.curSrc=a1,c.supported=!(k||n&&!aL.supSrcset||m),l&&aL.supSrcset&&!c.supported&&(o?(aH.call(r,az,o),r.srcset=""):aG.call(r,az)),c.supported&&!c.srcset&&(!c.src&&r.src||r.src!==aL.makeUrl(c.src))&&(null===c.src?r.removeAttribute("src"):r.src=c.src),c.parsed=!0
},aL.fillImg=function(f,e){var h,g=e.reselect||e.reevaluate;
f[aL.ns]||(f[aL.ns]={}),h=f[aL.ns],(g||h.evaled!==aM)&&((!h.parsed||e.reevaluate)&&aL.parseSets(f,f.parentNode,e),h.supported?h.evaled=aM:aW(f))
},aL.setupRun=function(){(!aj||ap||am!==a3.devicePixelRatio)&&aY()
},aL.supPicture?(aC=aK,aL.fillImg=aK):!function(){var n,m=a3.attachEvent?/d$|^c/:/d$|^c|^i/,l=function(){var c=a2.readyState||"";
k=setTimeout(l,"loading"===c?200:999),a2.body&&(aL.fillImgs(),n=n||m.test(c),n&&clearTimeout(k))
},k=setTimeout(l,a2.body?9:99),j=function(g,f){var o,i,h=function(){var c=new Date-i;
f>c?o=setTimeout(h,f-c):(o=null,g())
};
return function(){i=new Date,o||(o=setTimeout(h,f))
}
},b=aF.clientHeight,a=function(){ap=Math.max(a3.innerWidth||0,aF.clientWidth)!==al.width||aF.clientHeight!==b,b=aF.clientHeight,ap&&aL.fillImgs()
};
ac(a3,"resize",j(a,99)),ac(a2,"readystatechange",l)
}(),aL.picturefill=aC,aL.fillImgs=aC,aL.teardownRun=aK,aC._=aL,a3.picturefillCFG={pf:aL,push:function(d){var c=d.shift();
"function"==typeof aL[c]?aL[c].apply(aL,d):(aB[c]=d[0],aj&&aL.fillImgs({reselect:!0}))
}};
for(;
at&&at.length;
){a3.picturefillCFG.push(at.shift())
}a3.picturefill=aC,"object"==typeof module&&"object"==typeof module.exports?module.exports=aC:"function"==typeof define&&define.amd&&define("picturefill",function(){return aC
}),aL.supPicture||(aE["image/webp"]=aZ("image/webp","data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA=="))
}(window,document);
(function(){if(typeof window.CustomEvent==="function"){return false
}function a(c,d){d=d||{bubbles:false,cancelable:false,detail:undefined};
var b=document.createEvent("CustomEvent");
b.initCustomEvent(c,d.bubbles,d.cancelable,d.detail);
return b
}a.prototype=window.Event.prototype;
window.CustomEvent=a
})();
(function(a){var b={};
function c(e){if(b[e]){return b[e].exports
}var d=b[e]={exports:{},id:e,loaded:false};
a[e].call(d.exports,d,d.exports,c);
d.loaded=true;
return d.exports
}c.m=a;
c.c=b;
c.p="";
return c(0)
})([function(b,a,c){b.exports=c(1)
},function(b,a,d){var c=d(2);
window.trace=function(e){};
window.app=new c()
},function(c,u,d){var i=d(3);
var a=d(4);
var h=d(5);
var l=d(6);
var f=d(7);
var b=d(8);
var g=d(9);
var t=d(10);
var j=d(11);
var s=d(15);
var p=d(16);
var k=d(17);
var q=d(18);
var o=d(19);
var m=d(20);
var n=d(21);
var e=d(22);
var r=function(){var B=document.getElementsByClassName("dropdown");
var K=document.getElementsByClassName("nav-drawer");
var E=document.getElementsByClassName("nav-drawer-menu");
var J=document.getElementsByClassName("nav-footer");
var L=document.getElementsByClassName("page-transition-container");
var F=document.getElementsByClassName("notification");
var A=document.getElementsByClassName("filter");
var C=[];
var w=document.getElementsByClassName("cd-item");
var z=document.getElementsByClassName("code-snippet");
var G=document.getElementsByClassName("module");
var H=document.querySelectorAll(".billboard.animate");
var M=document.querySelectorAll(".back-to-top");
var x=document.getElementsByClassName("nav-contextual-mobile");
var D=document.querySelectorAll(".equal-heights");
var P=document.getElementsByClassName("upload-button");
if(B.length>0){Array.prototype.forEach.call(B,function(S,R){var Q=a(S);
Q._public()
})
}if(K.length>0){var O=h();
O._public()
}if(E.length>0){var O=l();
O._public()
}if(H.length>0){Array.prototype.forEach.call(H,function(R,Q){var S=f();
S._public(R)
})
}if(J.length>0){var v=b();
v._public()
}function y(R){var T=R;
if(T.length>0){Array.prototype.forEach.call(T,function(W,V){var U=t(W);
U._public();
C.push(U)
});
document.addEventListener("click",function(Y){for(var W=0;
W<C.length;
W++){var V=C[W];
var U=V.getFilterOpened();
var X=V.getEl();
if(U&Y.target!==X&&!X.contains(Y.target)){V.close()
}}});
var S=document.querySelectorAll(".filtered-modules .module");
var Q=j(T,S,true)
}}y(A);
if(w.length>0){Array.prototype.forEach.call(w,function(S,R){var Q=s(S);
Q._public()
})
}if(z.length>0){Array.prototype.forEach.call(z,function(R,Q){var S=p(R);
S._public()
})
}if(L.length>0){var I=g();
I._public()
}if(G.length>0){var O=k();
O._public()
}if(F.length>0){Array.prototype.forEach.call(F,function(S,R){var Q=q(S);
Q._public(S)
})
}if(M.length>0){Array.prototype.forEach.call(M,function(S,R){var Q=o(S);
Q._public()
})
}if(x.length>0){var N=m(x[0]);
N._public()
}if(D.length>0){Array.prototype.forEach.call(D,function(R,Q){var S=n();
S._public(R)
})
}if(P.length>0){Array.prototype.forEach.call(P,function(S,R){var Q=e(S);
Q._public()
})
}fluidvids.init();
return{bindFilters:y}
};
c.exports=r
},function(b,a,c){(function(d,e){if(true){e(a)
}else{if(typeof define==="function"&&define.amd){define(["exports"],e)
}else{e(d)
}}})(this,function(f){var n="CEROS_SCROLL_PROXY_LOADED";
if(window[n]){return
}window[n]=true;
var d="ceros-embedded-viewport:";
var l={PING:"ping",READY:"ready",POSITION:"position",SCROLL_TO:"scroll-to"};
var k=[];
var h=function(t){var i=t.getBoundingClientRect();
var s={top:-i.top,bottom:window.innerHeight-i.top};
q(t,l.POSITION,s)
};
var e=function(){for(var s=0;
s<k.length;
s++){h(k[s])
}};
var r=function(u,v){var t=null;
for(var s=0;
s<u.length;
s++){if(u[s].contentWindow===v){t=u[s];
break
}}return t
};
var j=function(i){var s=r(document.getElementsByTagName("iframe"),i);
if(s){k.push(s);
h(s)
}};
var m=function(z,w){var s=r(k,z);
if(s){var i=Math.max(0,w.scrollPosition-w.pageHeight+w.visibleHeight);
var t=s.getBoundingClientRect();
var u=window.pageYOffset+t.top;
var x=window.pageYOffset+t.bottom;
var v=t.bottom-t.top;
var A=i*v/w.visibleHeight;
var y=x-window.innerHeight;
window.scroll(0,Math.min(A+u,y))
}};
var o=function(i){var s,u;
try{s=JSON.parse(i.data);
u=s&&s.event&&s.event.substr(0,d.length)===d
}catch(t){return
}if(u){switch(s.event.substr(d.length)){case l.READY:j(i.source);
break;
case l.SCROLL_TO:m(i.source,s);
break;
default:}}};
var q=function(v,s,t){var i={event:d+s};
if(t){for(var w in t){if(t.hasOwnProperty(w)){i[w]=t[w]
}}}var u=JSON.stringify(i);
v.contentWindow.postMessage(u,"*")
};
var p=document.getElementsByTagName("iframe");
for(var g=0;
g<p.length;
g++){if(k.indexOf(p[g]===-1)){q(p[g],l.PING)
}}window.sendViewportPositionEvent=h;
window.addEventListener("message",o);
window.addEventListener("scroll",e);
window.addEventListener("resize",e)
})
},function(b,a){function c(d){var f=document.getElementsByClassName("error-message");
function g(){}var e=function(k,h){var i=[];
if(h){var j=h.charAt(0)
}for(;
k&&k!==document;
k=k.parentNode){if(h){if(j==="."){if(k.classList.contains(h.substr(1))){i.push(k)
}}if(j==="#"){if(k.id===h.substr(1)){i.push(k)
}}if(j==="["){if(k.hasAttribute(h.substr(1,h.length-1))){i.push(k)
}}if(k.tagName.toLowerCase()===h){i.push(k)
}}else{i.push(k)
}}if(i.length===0){return null
}else{return i
}};
return{_public:function(){g();
var n=false;
var q=d.getElementsByClassName("title");
var l=d.getElementsByTagName("input")[0];
var t=q[0].getElementsByTagName("span");
var h=d.getElementsByTagName("ul");
var m=h[0].getElementsByTagName("li");
var s=t[0].innerHTML;
var x=false;
var j=!!$(d).closest(".filterable").length;
function v(y){var i=e(d,".dropdown-container");
if(i[0].classList.contains("disabled")){return
}u(d,"closed")
}TwitterAEM.components=TwitterAEM.components||{};
TwitterAEM.components.dropdown=TwitterAEM.components.dropdown||{};
TwitterAEM.components.dropdown.handlers=TwitterAEM.components.dropdown.handlers||{};
TwitterAEM.components.dropdown.handlers.titleSelection=function(i){v(d)
};
q[0].addEventListener("click",TwitterAEM.components.dropdown.handlers.titleSelection);
function r(A){var y=A.parentNode.childNodes;
var B=0;
for(var z=0;
z<y.length;
z++){if(y[z]==A){return B
}if(y[z].nodeType==1){B++
}}return -1
}function w(i){if(i>=0&&i<this.length){return this[i]
}else{return -1
}}for(var o=0;
o<m.length;
o++){m[o].addEventListener("click",p);
if(!j){$(m[o]).on("itemchange",k)
}var p=function(A){var z=this;
if(r(z)!=0&&r(z)!=m.length-1){A.preventDefault();
var i=z.innerHTML;
var B=z.dataset.val;
t[0].innerHTML=i;
l.value=B;
l.setAttribute("checked","checked");
$(l).trigger("change");
u(d,"closed");
q[0].classList.remove("error");
if(i!=s&&x===false){var y=h[0].querySelector(".hidden");
if(y){y.style.display="block"
}x=true
}}};
var k=function(C){var B=this;
if(r(B)!=0&&r(B)!=m.length-1){C.preventDefault();
var E=$(B).offset().top-20,y=$(h).offset().top,i=$(h).scrollTop();
$(h).scrollTop(E-y+i);
var z=B.innerHTML;
var D=B.dataset.val;
t[0].innerHTML=z;
l.value=D;
l.setAttribute("checked","checked");
$(l).trigger("change");
q[0].classList.remove("error");
if(z!=s&&x===false){var A=h[0].querySelector(".hidden");
if(A){A.style.display="block"
}x=true
}}}
}$(d).on("dropdownOpen",function(){var y=$(this);
var i,z="";
y.keypress(function(C){C.preventDefault();
var B=String.fromCharCode(C.which);
if(C.which==32){u(d,"closed")
}if(i){z+=B
}else{z=B
}var A=new RegExp("^"+z,"i");
y.find('li:not(".hidden")').removeClass("active").filter(function(){return A.test($(this).text())
}).first().addClass("active").trigger("itemchange");
i=setTimeout(function(){z="";
i=null
},500)
});
y.on("dropdownClose",function(){y.unbind("keypress")
})
});
document.addEventListener("click",function(i){if(n==true&&i.target!==d&&!d.contains(i.target)){d.classList.add("closed");
n=false;
if(!j){$(d).trigger("dropdownClose")
}}});
function u(y,i){y.classList.toggle(i);
if(n==false){n=true;
if(!j){$(y).trigger("dropdownClose");
$(y).trigger("dropdownOpen")
}}else{n=false
}}}}
}b.exports=c
},function(b,a){function c(d){function e(){}return{_public:function(){e();
var i=document.getElementsByClassName("nav-drawer");
var t=document.getElementsByClassName("masthead");
var l=document.getElementsByClassName("signed-in_dropdown");
var f=document.getElementsByClassName("avatar");
var m=document.getElementsByClassName("search");
var v=document.querySelector(".search input");
var h=document.getElementById("loader");
var u=document.getElementsByClassName("sticky-cta");
var k=document.querySelector(".nav-contextual-mobile");
var g=document.querySelector(".nav-contextual-mobile .current > a");
var j=false;
var r=false;
var q=false;
if(u.length>0){i[0].classList.add("with-cta");
if(typeof t[0]!=="undefined"){t[0].classList.add("with-cta")
}}function p(){if(typeof h!=="undefined"&&h!==null){h.classList.remove("active")
}if(typeof t[0]!=="undefined"){t[0].classList.remove("animate")
}if(typeof i[0]!=="undefined"){i[0].classList.remove("animate")
}}setTimeout(function(){p()
},1000);
for(var n=0;
n<i.length;
n++){(function(y){var z=i[n].getElementsByClassName("nav-drawer-link")[0];
z.addEventListener("click",function(F){var E=o(this,".nav-drawer");
if(E[0].classList.contains("no-interaction")){return
}else{F.preventDefault();
s(i[y],"active");
s(w,"active")
}return false
});
var w=i[n].getElementsByClassName("nav-icon")[0];
if(typeof w!=="undefined"){w.addEventListener("click",function(E){E.preventDefault();
s(i[y],"active");
s(w,"active");
return false
})
}var D=i[n].getElementsByClassName("avatar");
var B=i[n].getElementsByClassName("signed-in_dropdown");
if(D&&D.length&&B&&B.length){D[0].addEventListener("click",function(E){E.preventDefault();
j=true;
s(signedInDropdown[0],"active");
return false
})
}var C=i[n].getElementsByClassName("search")[0];
var x=i[n].getElementsByClassName("search-icon")[0];
var A=0;
if(typeof x!=="undefined"){x.addEventListener("click",function(E){E.preventDefault();
r=true;
C.classList.add("opened");
A++;
v.focus();
return false
})
}document.addEventListener("click",function(E){if(A>1&&v.value!=""){C.submit()
}A++;
if(E.target!==i[y]&&!i[y].contains(E.target)){i[y].classList.remove("active");
if(w){w.classList.remove("active")
}}})
})(n)
}var o=function(z,w){var x=[];
if(w){var y=w.charAt(0)
}for(;
z&&z!==document;
z=z.parentNode){if(w){if(y==="."){if(z.classList.contains(w.substr(1))){x.push(z)
}}if(y==="#"){if(z.id===w.substr(1)){x.push(z)
}}if(y==="["){if(z.hasAttribute(w.substr(1,w.length-1))){x.push(z)
}}if(z.tagName.toLowerCase()===w){x.push(z)
}}else{x.push(z)
}}if(x.length===0){return null
}else{return x
}};
document.addEventListener("click",function(w){if(j==true&&w.target!==f[0]&&!f[0].contains(w.target)&&w.target!==l[0]&&!l[0].contains(w.target)){l[0].classList.remove("active");
j=false
}if(r==true&&w.target!==m[0]&&!m[0].contains(w.target)){v.value="";
m[0].classList.remove("opened");
r=false;
q=false
}});
if(typeof g!=="undefined"&&g!==null){g.addEventListener("click",function(w){w.preventDefault();
s(k,"active");
return false
})
}function s(z,y){if(z.classList){z.classList.toggle(y)
}else{var x=z.className.split(" ");
var w=x.indexOf(y);
if(w>=0){x.splice(w,1)
}else{x.push(y)
}z.className=x.join(" ")
}}}}
}b.exports=c
},function(b,a){function c(d){function e(){}return{_public:function(){e();
var i=document.querySelector(".nav-drawer-menu");
var h=i.querySelector(".signed-in_dropdown");
var g=i.querySelector(".avatar");
if(typeof g!=="undefined"&&g!==null){g.addEventListener("click",function(j){j.preventDefault();
f(h,"active");
return false
})
}function f(m,l){if(m.classList){m.classList.toggle(l)
}else{var k=m.className.split(" ");
var j=k.indexOf(l);
if(j>=0){k.splice(j,1)
}else{k.push(l)
}m.className=k.join(" ")
}}}}
}b.exports=c
},function(b,a){function c(d){function e(){}return{_public:function(f){e();
window.setTimeout(function(){f.classList.remove("animate")
},3000)
}}
}b.exports=c
},function(b,a){function c(f){var i=document.getElementsByClassName("language")[0];
var e=document.getElementsByClassName("lang-selection")[0];
var d=document.getElementsByClassName("lang-flyout")[0];
var g=false;
function h(){}return{_public:function(){h();
e.addEventListener("click",function(k){k.preventDefault();
if(g==false){g=true;
i.classList.add("active")
}else{g=false;
i.classList.remove("active")
}return false
});
document.addEventListener("click",function(k){if(g==true&&k.target!==d&&!d.contains(k.target)&&k.target!==e&&!e.contains(k.target)){i.classList.remove("active");
g=false
}});
function j(n,m){if(n.classList){n.classList.toggle(m)
}else{var l=n.className.split(" ");
var k=l.indexOf(m);
if(k>=0){l.splice(k,1)
}else{l.push(m)
}n.className=l.join(" ")
}}}}
}b.exports=c
},function(c,b){function a(h){var e=document.getElementById("loader");
var f=document.getElementById("first");
var d=document.getElementById("second");
var g;
var j=false;
function i(){}return{_public:function(){i();
var m=new XMLHttpRequest();
m.open("GET","page-transition-content-1.html",true);
m.onload=function(){if(this.status>=200&&this.status<400){f.innerHTML=this.response
}else{}};
m.send();
setTimeout(function(){e.classList.add("active")
},2000);
setTimeout(function(){first.classList.add("active");
e.classList.remove("active")
},4500);
k();
d.style.left=g+"px";
f.addEventListener("click",function(n){setTimeout(function(){e.classList.add("active")
},1000);
l()
});
function l(){first.classList.remove("active");
var n=new XMLHttpRequest();
n.open("GET","page-transition-content-2.html",true);
n.onload=function(){if(this.status>=200&&this.status<400){d.innerHTML=this.response
}else{}};
n.send();
setTimeout(function(){f.style.left=-g+"px";
d.style.left="0px";
e.classList.remove("active")
},3000);
setTimeout(function(){info.classList.add("active")
},4000);
j=true
}function k(){g=window.innerWidth;
e.width=g+"px";
if(j==false){d.style.left=g+"px"
}}window.addEventListener("resize",function(n){k()
})
}}
}c.exports=a
},function(b,a){function c(g){var f=[];
var k=null;
var d=null;
var l=null;
var i=null;
var j=false;
function p(){}function o(q){var q=q;
j=false;
k=q.getElementsByClassName("title");
d=q.getElementsByTagName("span");
l=q.getElementsByTagName("ul");
i=q.getElementsByTagName("li")
}function h(){window.addEventListener("resize",function(r){e()
});
k[0].addEventListener("click",function(r){m(g,"closed")
});
function q(u){var s=$(u),t=s.closest(".filter").find(".title span").first(),r=s.siblings("label").first().text();
t.text(r)
}$(g).on("itemchange",'input[type="radio"]',function(){q(this)
});
$(g).on("dropdownOpen",function(){var s=$(this);
var r,t="";
s.keypress(function(w){w.preventDefault();
var v=String.fromCharCode(w.which);
if(w.which==32){m(g,"closed")
}if(r){t+=v
}else{t=v
}var u=new RegExp("^"+t,"i");
s.find("li").removeClass("active").filter(function(){return u.test($(this).find("label").first().text())
}).first().addClass("active").find('input[type="radio"]').prop("checked",true).trigger("itemchange");
r=setTimeout(function(){t="";
r=null
},500)
});
s.on("dropdownClose",function(){s.unbind("keypress")
})
})
}function n(){g.classList.add("closed");
j=false;
$(g).trigger("dropdownClose")
}function m(r,q){r.classList.toggle(q);
if(j==false){j=true;
$(r).trigger("dropdownClose");
$(r).trigger("dropdownOpen")
}else{j=false
}}function e(){}return{_public:function(){o(g);
h()
},getFilterOpened:function(){return j
},getEl:function(){return g
},close:function(){n()
},resize:function(){e()
}}
}b.exports=c
},function(c,b,e){var d=e(12);
var f=e(14);
function a(k,v,A){A=A||false;
var s=[],r=null,p=null,j=3,u=[],C=500,g=false,B=false,D=120,x;
function E(){var L=/^simulate-all-/,K=[],J=[],H=document.getElementsByTagName("input"),I,G;
for(I=0,G=H.length;
I<G;
I+=1){if(L.test(H[I].id)){K.push(H[I]);
J.push(H[I])
}}}function F(i){if(g&&!B&&i===1||!g&&B&&i===2){return true
}return false
}function m(){var i;
if(g&&!B){i=1
}else{if(!g&&B){i=2
}}return i
}function l(){var H=C,G=0,J,K;
function L(O,M,i){var N=m();
setTimeout(function(){if(!F(N)){return
}O.itemEl.style.display="inline-block";
if(A){f(O.itemEl,"no-margin");
if(i%j===0){d(O.itemEl,"no-margin")
}else{d(O.itemEl,"active")
}}},M-50);
setTimeout(function(){if(!F(N)){return
}O.itemEl.style.opacity=1
},M)
}function I(N,i){var M=m();
setTimeout(function(){if(!F(M)){return
}N.itemEl.style.display="none"
},i)
}if(!g&&!B){g=true
}else{if(g&&!B){g=false;
B=true
}else{if(!g&&B){g=true;
B=false
}}}for(J=0;
J<u.length;
J+=1){K=u[J];
K.itemEl.style.opacity=0;
if(K.active){G+=1;
L(K,H,G);
H+=D
}else{I(K,C)
}}}function z(){var H="",M=false,L,I,G,N=function(R){var O=true,i,Q,S,P;
for(L=0;
L<s.length;
L+=1){i=s[L];
Q=true;
if(i.active.length){Q=false;
for(I=0;
I<i.active.length;
I+=1){S=i.active[I];
for(G=0;
G<R.filters.length;
G+=1){P=R.filters[G];
if(P===S){Q=true
}}}if(Q===false){O=false
}}}return O
},K=function(){var Q,P,O;
if(u.length===0){M=true
}for(Q=0;
Q<u.length;
Q+=1){P=u[Q];
O=N(P);
if(O){P.active=true
}else{P.active=false
}if(Q===u.length-1){M=true
}}},J=[];
do{K()
}while(!M);
l();
setTimeout(function(){var i=null;
var O="filterAnimationComplete";
i=new CustomEvent(O);
document.dispatchEvent(i)
},C)
}function y(){var J,I,H,G,K;
for(J=0;
J<s.length;
J+=1){H=s[J];
H.active=[];
for(I=0;
I<H.inputsEl.length;
I+=1){G=H.inputsEl[I];
K=G.getAttribute("value");
if(K!==null&&K!==""&&G.checked){H.active.push(K)
}if(K!==null&&G.checked){$(H.inputsEl[I]).trigger("change")
}}if(H.active.length){H.tracker=0
}}z()
}function n(i){i.addEventListener("change",function(G,H){y()
})
}function h(){var G;
for(G=0;
G<r.length;
G+=1){n(r[G])
}}function t(){var G;
for(G=0;
G<s.length;
G+=1){s[G].active=[]
}z()
}function o(){var i=window.location.hash.substring(1).split(",");
if(i.length>0){i.forEach(function(H){var G=document.getElementById(H);
if(G){G.checked=true
}});
y()
}}function q(){}function w(H){var K,J,G,O,L,I,N,M,P;
r=H;
p=v;
for(K=0;
K<r.length;
K+=1){G=r[K];
s.push({active:[],tracker:false,inputsEl:G.getElementsByTagName("input")})
}for(K=0;
K<p.length;
K+=1){O=p[K];
I=O.dataset.tags;
N=[];
if(typeof I!=="undefined"){M=I.split(",");
M=M.filter(Boolean);
for(J=0;
J<M.length;
J+=1){P=M[J].split("/");
if(P.length>1){L=P[1].split("+")
}if(L.length>1){P[1]=L
}N.concat(L);
Array.prototype.push.apply(N,L)
}N=M
}u.push({active:true,filters:N,itemEl:p[K]})
}E();
o()
}w(k);
h();
return{_public:function(){},resetFilters:function(){t()
},resize:function(){q()
}}
}c.exports=a
},function(c,a,e){var b=e(13);
function d(g,f){if(g.classList){g.classList.add(f)
}else{if(!b(g,f)){g.className+=" "+f
}}}c.exports=d
},function(c,a){function b(e,d){if(e.classList){return e.classList.contains(d)
}else{return !!e.className.match(new RegExp("(\\s|^)"+d+"(\\s|$)"))
}}c.exports=b
},function(c,a,d){var b=d(13);
function e(h,g){if(h.classList){h.classList.remove(g)
}else{if(b(h,g)){var f=new RegExp("(\\s|^)"+g+"(\\s|$)");
h.className=h.className.replace(f," ")
}}}c.exports=e
},function(c,b){function a(e){var f=e.querySelector("h5");
var h=e.querySelectorAll(".cdi-content");
var d=e.querySelectorAll(".arrow-down");
function g(){}return{_public:function(){g();
f.addEventListener("click",function(i){e.classList.toggle("opened");
i.preventDefault()
})
}}
}c.exports=a
},function(b,a){function c(g){var f=g.getElementsByClassName("codelinecopybtn");
var e=g.getElementsByClassName("cs-code");
var d=e[0].getElementsByTagName("code");
function h(){}return{_public:function(){h();
Array.prototype.forEach.call(d,function(r,q){k(r)
});
Array.prototype.forEach.call(f,function(r,q){if(document.queryCommandSupported("copy")){r.addEventListener("click",function(s){m(r)
})
}else{ZeroClipboard.config({swfPath:"/etc/designs/common-twitter/clientlib-site/swf/zeroclipboard.swf",cacheBust:true});
o(r)
}});
var j=g;
Array.prototype.forEach.call(e,function(u,t){var s=j.classList.contains("code-snippet-collapsed");
var q=u.querySelectorAll(".cs-code-line").length;
if(q>16&&!s){u.classList.add("reduced");
u.parentElement.classList.add("grad");
for(var t=16;
t<q;
t++){u.querySelectorAll(".cs-code-line")[t].classList.add("hidden")
}var r=u.parentNode.querySelector(".see");
r.classList.add("active");
r.classList.add("more");
r.addEventListener("click",function(w){if(this.classList.contains("more")){this.getElementsByTagName("span")[0].innerHTML="See Less";
for(var v=16;
v<q;
v++){u.querySelectorAll(".cs-code-line")[v].classList.remove("hidden")
}u.parentElement.classList.remove("grad");
this.classList.remove("more");
this.classList.add("less")
}else{this.getElementsByTagName("span")[0].innerHTML="See More";
for(var v=16;
v<q;
v++){u.querySelectorAll(".cs-code-line")[v].classList.add("hidden")
}this.classList.remove("less");
this.classList.add("more");
u.parentElement.classList.add("grad")
}})
}});
function k(u){var r=l(u,"cs-code");
var v=u.innerHTML;
var q=v.split("\n");
u.style.display="none";
if(u.parentElement.nodeName=="PRE"){u.parentElement.style.display="none"
}for(var t=0;
t<q.length;
t++){var s=t+1;
if(s<10){s="0"+s
}n(s,q[t],r)
}}function n(s,w,r){var u=document.createElement("code");
u.innerHTML=w;
var v=document.createElement("pre");
v.appendChild(u);
var q=document.createElement("div");
q.className="number-line";
q.innerHTML=s;
var t=document.createElement("div");
t.className="cs-code-line";
t.appendChild(q);
t.appendChild(v);
r.appendChild(t)
}function o(t){var v,r,u;
var s;
r="";
v=t.parentNode.previousElementSibling.querySelectorAll("div.cs-code-line code");
if(v.length>0){for(s=0;
s<v.length;
s++){r=r+v[s].textContent+" \n"
}var q=new ZeroClipboard(t);
q.on("ready",function(w){q.on("copy",function(x){x.clipboardData.setData("text/plain",r)
});
q.on("aftercopy",function(x){})
});
q.on("error",function(w){ZeroClipboard.destroy()
})
}}function m(w){var s,q,t;
var v;
q="";
s=w.parentNode.previousElementSibling.querySelectorAll("div.cs-code-line code");
if(s.length>0){for(v=0;
v<s.length;
v++){q=q+s[v].textContent+" \n"
}var y=Math.floor(new Date().getTime()/1000);
var r="cs_code_copy_"+y;
t=document.createElement("textarea");
t.id=r;
t.style.position="fixed";
t.style.opacity=0;
t.value=q;
document.body.appendChild(t);
document.getElementById(r).focus();
document.getElementById(r).select();
try{var x=document.execCommand("copy")
}catch(u){}finally{document.body.removeChild(t)
}}}function l(r,q){while((r=r.parentElement)&&!r.classList.contains(q)){}return r
}if(g.classList.contains("code-snippet-collapsed")){var i=g.getElementsByClassName("close-snippet")[0];
var p=g.getElementsByTagName("h5")[0];
i.addEventListener("click",function(q){g.classList.toggle("opened")
});
p.addEventListener("click",function(q){g.classList.toggle("opened")
})
}}}
}b.exports=c
},function(c,a){function b(d){function e(){}return{_public:function(){e()
}}
}c.exports=b
},function(b,a){function c(d){function e(){}return{_public:function(){e();
d.addEventListener("click",function(f){f.preventDefault();
this.classList.remove("active");
return false
})
}}
}b.exports=c
},function(c,a){function b(d){function e(){}return{_public:function(){e();
g();
d.addEventListener("click",function(j){var i=document.documentElement.scrollTop>0?document.documentElement:document.body;
f(i,0,1000)
});
var h=null;
d.addEventListener("mouseover",function(j){var i=document.documentElement.scrollTop>0?document.documentElement.scrollTop:document.body.scrollTop;
if(i>400){clearTimeout(h);
d.classList.add("reveal")
}else{d.classList.remove("reveal")
}});
document.addEventListener("scroll",function(){clearTimeout(h);
var i=document.documentElement.scrollTop>0?document.documentElement.scrollTop:document.body.scrollTop;
if(i>400){d.classList.add("reveal");
g()
}else{d.classList.remove("reveal")
}h=setTimeout(function(){d.classList.remove("reveal")
},500)
});
function g(){var j=document.querySelector("footer");
var i=d.querySelector("button");
if(i.getBoundingClientRect().top>=j.getBoundingClientRect().top-200){i.classList.add("absolute")
}if(j.getBoundingClientRect().top-window.innerHeight>10){i.classList.remove("absolute")
}}function f(k,p,m){var o=k.scrollTop,n=p-o,l=0,i=20;
var j=function(){l+=i;
var q=Math.easeOutQuart(l,o,n,m);
k.scrollTop=q;
if(l<m){setTimeout(j,i)
}};
j()
}Math.easeOutQuart=function(j,i,l,k){j/=k;
j--;
return -l*(j*j*j*j-1)+i
}
}}
}c.exports=b
},function(c,b){function a(d){function e(){}return{_public:function(){e();
var f=document.getElementsByClassName("transparent");
var h=function(l,i){var j=[];
if(i){var k=i.charAt(0)
}for(;
l&&l!==document;
l=l.parentNode){if(i){if(k==="."){if(l.classList.contains(i.substr(1))){j.push(l)
}}if(k==="#"){if(l.id===i.substr(1)){j.push(l)
}}if(k==="["){if(l.hasAttribute(i.substr(1,i.length-1))){j.push(l)
}}if(l.tagName.toLowerCase()===i){j.push(l)
}}else{j.push(l)
}}if(j.length===0){return null
}else{return j
}};
var g=h(d,".t-row");
if(g!==null){g[0].classList.add("t-row-contextual");
if(f.length>0){g[0].classList.add("transp")
}}}}
}c.exports=a
},function(c,a){function b(d){function e(){}return{_public:function(i){e();
var f;
var g=function(){var k=0;
for(var l=0;
l<f.length;
l++){var j=f[l].offsetHeight;
if(j>k){k=j
}}return k
};
var h=function(){for(var k=0;
k<f.length;
k++){if(!f[k].classList.contains("t06-timeline")){f[k].style.height="auto"
}else{f[k].style.height="600px"
}}if(window.innerWidth>900){var j=g();
for(var k=0;
k<f.length;
k++){f[k].style.height=j+"px"
}}};
f=i.querySelectorAll(".comp-wrapper");
if(f.length>1){window.addEventListener("resize",h);
h()
}window.onload=function(){h()
}
}}
}c.exports=b
},function(c,a){function b(d){function e(){}return{_public:function(){e();
var g=d.querySelector(".input-file"),i=g.nextElementSibling,l=i.textContent,f=i.querySelector("span"),k=i.querySelector(".upload-button-close"),h=d.querySelector(".error-message");
g.addEventListener("change",function(m){var n=j();
k.classList.remove("hide");
if(n){f.textContent=n;
i.classList.add("file-added");
if(h){h.classList.remove("active");
i.classList.remove("error")
}}else{f.textContent=l;
i.classList.remove("file-added")
}});
k.addEventListener("click",function(m){m.preventDefault();
g.value=null;
f.textContent=l;
i.classList.remove("file-added");
k.classList.add("hide")
});
function j(){var m;
m=g.value.split("\\").pop();
return m
}}}
}c.exports=b
}]);
(function(d){if(d(".c15-content-dropdown").length>0){var c=location.hash,b,a=false;
a=c.indexOf("?")!==-1;
c=c.split("?")[0];
b=d(c);
if(b.length){b.closest(".cd-item").addClass("opened");
if(a){d("html, body").animate({scrollTop:b.offset().top})
}}}})(jQuery);
(function(c,a,b){a.components.initUserProfile=function(){var d=c(".signed-in_dropdown");
if(d.length>0){a.user={screenName:d.data("profileScreenName"),fullName:d.data("profileFullName"),email:d.data("profileEmail"),location:d.data("profileLocation"),language:d.data("profileLanguage")}
}};
c(document).ready(function(){var e=c(".sign-in a");
var d="https://twitter.com/login?redirect_after_login="+b.location.href;
e.attr("href",d);
var f="https://twitter.com/logout?redirect_after_logout="+b.location.href;
c(".signed-in_dropdown a[href='https://twitter.com/logout']").attr("href",f)
})
})(jQuery,TwitterAEM,window);
(function(c,a,b){a.components.buildTimelines=function(){var d=c(".t06-timeline");
if(typeof twttr!=="undefined"&&d.length>0){d.each(function(e,f){twttr.widgets.createTimeline(f.dataset.timelineId,f,{width:f.dataset.timelineWidth,height:f.dataset.timelineHeight,borderColor:f.dataset.timelineBorderColor,chrome:f.dataset.timelineChrome,ariaPolite:f.dataset.timelineAriaPolite,tweetLimit:f.dataset.timelineTweetLimit,screenName:f.dataset.timelineScreenName,userId:f.dataset.timelineUserId,showReplies:f.dataset.timelineShowReplies,favoritesScreenName:f.dataset.timelineFavoritesScreenName,listOwnerScreenName:f.dataset.timelineListOwnerScreenName,listOwnerId:f.dataset.timelineListOwnerId,listId:f.dataset.timelineListId,listSlug:f.dataset.timelineListSlug,customTimelineId:f.dataset.timelineCustomTimelineId})
})
}}
})(jQuery,TwitterAEM,window);
(function(c,a,b){a.components.buildCollectionTimelines=function(){var d=c(".t07-collection-timeline");
if(typeof twttr!=="undefined"&&d.length>0){d.each(function(e,f){twttr.widgets.createGridFromCollection(f.dataset.collectionTimelineWidgetId,f,{limit:f.dataset.collectionTimelineLimit})
})
}}
})(jQuery,TwitterAEM,window);
(function(c,a,b){a.components.buildTweets=function(){var d=c(".t08-tweet");
if(typeof twttr!=="undefined"&&d.length>0){d.each(function(e,f){twttr.widgets.createTweet(f.dataset.tweetId,f,{width:f.dataset.tweetWidth,conversation:f.dataset.tweetConversation,cards:f.dataset.tweetCards,linkColor:f.dataset.tweetLinkColor,theme:f.dataset.tweetTheme})
})
}}
})(jQuery,TwitterAEM,window);
(function(c,a,b){a.components.buildTwitterShare=function(){var d=c(".t12-twitter-share");
if(typeof twttr!=="undefined"&&d.length>0){d.each(function(e,f){var h=f.dataset.twitterShareUrl;
var g=f.dataset.twitterUseHyperRef;
if(g){h=b.location.href
}twttr.widgets.createShareButton(h,f,{lang:f.dataset.twitterShareLang,size:f.dataset.twitterShareSize,url:h,via:f.dataset.twitterShareVia,related:f.dataset.twitterShareRelated,text:f.dataset.twitterShareText,hashtags:f.dataset.twitterShareHashtags,dnt:f.dataset.twitterShareDnt})
})
}}
})(jQuery,TwitterAEM,window);
(function(c,a,b){a.components.buildTwitterFollow=function(){var d=c(".t13-twitter-follow");
if(typeof twttr!=="undefined"&&d.length>0){d.each(function(f,g){var e=g.dataset.twitterFollowScreenName;
if(e.indexOf("@")!==-1){e=e.replace("@","")
}twttr.widgets.createFollowButton(e,g,{size:g.dataset.twitterFollowSize,showCount:g.dataset.twitterFollowShowCount,showScreenName:g.dataset.twitterFollowShowScreenName,dnt:g.dataset.twitterFollowDnt})
})
}}
})(jQuery,TwitterAEM,window);
(function(c,a,b){a.components.modifyTweets=function(){intervalHandle=setInterval(function(){var d=c(".t08-tweet").not(".g-modified");
if(d.length===0){clearInterval(intervalHandle)
}d.each(function(){var m=c(this),k=m.find(".twitter-tweet"),j=k.contents().find("blockquote.tweet").last(),i="",e="",g="",l="",f="",h="";
j.find('.customisable[title*="cards.twitter.com"]').addClass("u-hiddenVisually");
j.find(".Button--smallGray").addClass("u-hiddenVisually");
if(m.parents(".node-type-blog").length!==1||m.hasClass("g-mask-date")){i=j.find(".dateline a");
i.text("View on Twitter");
m.addClass("g-modified")
}if(m.hasClass("promoted-tweet")){h=m.hasClass("promoted-tweet-political")?"promoted-political.png":"promoted.png";
e=j.find(".TweetAuthor-link").attr("href");
g=m.find(".TweetAuthor-name").text();
if(!g){if(j.find(".header .full-name .p-name")[0]){g=j.find(".header .full-name .p-name").text()
}else{g=j.find(".Identity-name").text()
}}l="Promoted by "+g;
f='<div class="context">';
f+='<span class="metadata with-icn">';
f+='<img class="promoted-img" src="/etc/designs/common-twitter/clientlib-site/imgs/'+h+'" />';
f+='<a href="'+e+'" class="js-action-profile-promoted js-user-profile-link js-promoted-badge">'+l+"</a>";
f+="</span>";
f+="</div>";
if(j.find(".footer")[0]){k.css("height",k.height()+15+"px");
j.find(".footer").prepend(f);
j.find(".footer .js-action-profile-promoted").css("padding-left","5px")
}else{k.css("height",k.height()+22+"px");
j.find(".Tweet-metadata").prepend(f);
j.find(".Tweet-metadata .js-action-profile-promoted").css("padding-left","5px")
}m.addClass("g-modified")
}})
},3000)
};
jQuery(document).ready(function(){a.components.modifyTweets()
})
})(jQuery,TwitterAEM,window);
(function(c,a,b){c(document).ready(function(){a.components.initUserProfile();
a.components.buildTimelines();
a.components.buildCollectionTimelines();
a.components.buildTweets();
a.components.buildTwitterShare();
a.components.buildTwitterFollow();
a.forms.list.forEach(function(d){new d()
})
})
})(jQuery,TwitterAEM,window);
var onLoadRecaptchaCallBack=function(){$(".g-recaptcha").each(function(b,c){var a=$(c).data("sitekey");
if(grecaptcha){var d=grecaptcha.render(c,{sitekey:a});
$(c).attr("data-widget-id",d)
}})
};
(function(){var a=function(){var u="data-",h="lg-",t="sm-",q="break-on",n="id",g="src",j="padding";
var o=[q,h+n,h+g,t+n,t+g];
var d=document.querySelectorAll("iframe.ceros-experience");
for(var s=0;
s<d.length;
s++){var e=d[s],l=e.parentNode;
var f=true;
for(var v=0;
v<o.length;
v++){var c=o[v],k=l.getAttribute(u+c);
if(k===null){f=false;
break
}}if(f){var m,p,b=l.getAttribute(u+q);
if(b=="touch"){m=("ontouchstart" in window||navigator.maxTouchPoints)
}else{var w=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,r=parseInt(b);
if(r){m=(w<r)
}else{continue
}}}else{continue
}if(m){p=u+t
}else{p=u+h
}e.setAttribute(g,l.getAttribute(p+g));
l.setAttribute(n,l.getAttribute(p+n))
}};
if(document.readyState!="loading"){a()
}else{if(document.addEventListener){document.addEventListener("DOMContentLoaded",a)
}else{document.attachEvent("onreadystatechange",function(){if(document.readyState=="complete"){a()
}})
}}})();
(function(c,j,f){f.twtrScribe=f.twtrScribe||{};
function b(q,p,o,n){if(!q){q=""
}if(!p){p=""
}if(!o){o=""
}if(!n){n=""
}return{section:q,action:p,element:o,component:n}
}function a(){let html=document.querySelector("html");
if(html&&html.hasAttribute("lang")){return html.getAttribute("lang")
}else{console.warn("twtrScribe could not read lang attribute");
return"no-lang"
}}function h(o,n){if(String.prototype.endsWith){return o.endsWith(n)
}else{let position=o.length-=n.length;
let lastIndex=o.indexOf(n,position);
return lastIndex!==-1&&lastIndex===position
}}function i(n){let hostname=f.location.host;
let site="undefined";
let hostnameComponents=hostname.split(".");
if(hostnameComponents.length==3&&h(hostname,".twitter.com")){site=hostnameComponents[0]
}else{if(hostnameComponents.length==2&&hostname=="twitter.com"){site="twitter"
}else{site=hostname.split(".").join("-")
}}return"aem-"+site+"-"+n
}function e(){let pageSlug=f.location.pathname.split("/").pop().split(".")[0];
if(pageSlug){return pageSlug
}else{return"/"
}}function g(){let url=f.location.origin+f.location.pathname;
return url
}function d(o,n){while(o&&n&&!o.classList.contains(n)){o=o.parentElement
}return o
}function k(){let seed1=(new Date()).getTime()%1000000000;
let seed2=Math.round(Math.random()*1000000000);
let sessionId=(seed1+seed2).toString(36).toUpperCase();
return sessionId
}function m(){if(typeof localStorage==="undefined"||localStorage==null){return"error-no-localstorage"
}let twtrScribe;
if(localStorage.getItem("twtrScribe")){twtrScribe=JSON.parse(localStorage.getItem("twtrScribe"))
}let currentTime=(new Date()).getTime();
let timeForNewSession=1800000;
if(!twtrScribe||!twtrScribe.sessionId||!twtrScribe.lastInteraction||currentTime>twtrScribe.lastInteraction+timeForNewSession){twtrScribe={};
twtrScribe.sessionId=k()
}twtrScribe.lastInteraction=currentTime;
localStorage.setItem("twtrScribe",JSON.stringify(twtrScribe));
return twtrScribe.sessionId
}function l(n,s,t,q,p,r,o){let time=(new Date()).getTime().toString();
let qParam=time.slice(-4);
let sessionId=m();
let logParam='[{"context":"'+sessionId+'","event_namespace":{"client":"'+n+'","page":"'+s+'","section":"'+t+'","element":"'+q+'","action":"'+r+'","component":"'+p+'"},"event_details":{"url":"'+o+'","triggered_on":'+time+'},"format_version":2,"_category_":"client_event"}]';
let absoluteUri="https://twitter.com/i/jot?q="+qParam+"&log="+encodeURIComponent(logParam);
let img=new Image();
img.src=absoluteUri
}f.twtrScribe.log=function(q,p,o,n){let validatedInput=b(q,p,o,n);
q=validatedInput.section;
p=validatedInput.action;
o=validatedInput.element;
n=validatedInput.component;
let lang=a();
let client=i(lang);
let pageSlug=e();
let url=g();
l(client,pageSlug,q,o,n,p,url)
};
f.twtrScribe.logTargetEl=function(p,o,n){let section;
let element;
let component;
p=d(p,o);
if(p){section=p.dataset.twtrScribeSection;
element=p.dataset.twtrScribeElement;
component=p.dataset.twtrScribeComponent
}f.twtrScribe.log(section,n,element,component)
}
})(jQuery,TwitterAEM,window);