(function(){var d=["stream","bar","radial"];
TwitterAEM.components=TwitterAEM.components||{};
TwitterAEM.components.graphs=TwitterAEM.components.graphs||{};
function c(l,f,j,i,h){var k=$.Deferred(),g=TwitterAEM.components.graphs[h+"s"][h][f]||{};
l(i,function(m,n){if(m){k.reject(m)
}else{g.data=g.data||{};
g.data[j]={src:i,data:n};
TwitterAEM.components.graphs[h+"s"][h][f]=g;
k.resolve(n)
}});
return k
}function a(m,f,l,i,j){var h,k,g=[];
k=l.find(".js-data-sources").first().data();
for(h in k){if(k.hasOwnProperty(h)){g.push(c(d3.csv,f,h,k[h],j))
}}$.when.apply(null,g).done(function(){m.call(this,arguments,i)
}).fail(function(n){console.error(n)
})
}function b(f){TwitterAEM.components.graphs[f+"s"]=TwitterAEM.components.graphs[f+"s"]||{};
TwitterAEM.components.graphs[f+"s"][f]=TwitterAEM.components.graphs[f+"s"][f]||[];
if(typeof TwitterAEM.components.graphs[f+"s"].initData!=="function"){TwitterAEM.components.graphs[f+"s"].initData=function(){var g=Array.prototype.slice.call(arguments);
g.push(f);
return a.apply(this,g)
}
}}function e(h){var g,f;
for(f=h.length-1;
f>=0;
f-=1){b(h[f])
}}e(d)
}());
(function(g){var f="switchChartsFromDropdown",e=this;
function d(j,i){e=this;
e.$wrapper=g(j);
e.options={classInputSelect:"js-select-value",attrIndicator:"data-dropdown-indicator",callback:null};
g.extend(e.options,i);
e.init()
}function c(k,j){var i=Array.prototype.slice.call(arguments);
i.push(j);
i.shift();
i.shift();
return k.apply(this,i)
}function b(i,j){j.$wrapper.find("["+j.options.attrIndicator+"]").each(function(){var k=g(this);
if(k.attr(j.options.attrIndicator)===i.target.value){k.removeAttr("hidden");
if(typeof j.options.callback==="function"){j.options.callback(k)
}else{throw"No callback provided."
}}else{k.attr("hidden","hidden")
}})
}function a(){e.$wrapper.find("."+e.options.classInputSelect).on("change",c.bind(undefined,b,e))
}g.extend(d.prototype,{init:function h(){a()
}});
g.fn[f]=function(i){return this.each(function(){if(!g.data(this,"plugin_"+f)){g.data(this,"plugin_"+f,new d(this,i))
}})
}
}(jQuery));
var BarChart;
(function(){var d={margin:{top:0,right:30,bottom:50,left:50},offset:[0,0],interpolate:"cardinal",gradientStops:[{offset:0,color:"rgba(255, 255, 255, 0.8)"},{offset:30,color:"rgba(255, 255, 255, 0)"}],xAccess:function c(f){return f.x
},yAccess:function e(f){return f.y
},lineWidth:1};
var a=["barMouseOver","barMouseMove","barMouseOut","barClick"];
BarChart=d3Kit.factory.createChart(d,a,function b(h){var p=h.options();
var j=h.getDispatcher();
var l=d3.scale.linear();
var k=d3.scale.ordinal();
var g=d3.svg.axis().scale(l).ticks(6,d3.format("")).tickPadding(10).tickSize(-5,1).orient("bottom");
var f=d3.svg.axis().scale(k).orient("left").tickPadding(20).tickSize(0,0);
var m=h.getRootG();
var i=m.append("g").classed("x",true).classed("axis",true);
var n=m.append("g").classed("y",true).classed("axis",true);
function o(){if(!(h.hasData()&&h.hasNonZeroArea())){return
}var q=h.data(),r=d3.max(q.map(p.xAccess));
l.domain([0,r+r*0.05]);
k.domain(q.map(p.yAccess));
l.range([0,h.getInnerWidth()]);
k.rangeRoundBands([0,h.getInnerHeight()],0.25);
i.attr("transform","translate(0,"+h.getInnerHeight()+")").call(g);
n.call(f);
var s=m.selectAll(".bar").data(q);
s.enter().append("rect").classed("bar",true).call(d3Kit.helper.bindMouseEventsToDispatcher,h.getDispatcher(),"bar",j);
s.exit().remove();
s.attr("x",function(u,t){return 1
}).attr("width",function(u,t){return l(p.xAccess(u,t))
}).attr("y",function(u,t){return k(p.yAccess(u,t))
}).attr("height",k.rangeBand())
}if(document.querySelectorAll(".wcmmode").length===0){h.autoResize("both").resizeToFitContainer("both").on("data",o).on("resize",o)
}else{h.resizeToFitContainer("both").on("data",o).on("resize",o)
}return h.mixin({visualize:o})
})
})();
(function(e){var a="barchart",l=this;
function b(u){var v;
if(typeof u==="undefined"){u=Math.random().toString(36).substr(2,5)
}v=u.replace(/[_\W]+/g,"");
return v
}function n(u){if(u!==null&&u!==undefined){return u.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")
}}function j(u){return Granite.I18n.get(u)
}function d(v,u){l=this;
l.id=b();
l.$wrapper=e(v);
l.$infobox=l.$wrapper.find(".info-box");
l.$barchart=l.$wrapper.find(".bar-chart");
l.barchartInit=false;
l.options={};
e.extend(l.options,u);
l.init()
}function s(){l.options.$config=l.$wrapper.find(".js-data-config");
l.options.graphs=l.options.$config.find(".js-graph").toArray().map(function(u){var v=e(u);
return{xAxisColumn:v.attr("data-x-axis"),yAxisColumn:v.attr("data-y-axis"),limiting_column:v.attr("data-limiting-column"),limiting_cellValue:v.attr("data-limiting-cell-value")}
});
l.options.skipRows=l.options.$config.find(".js-skip-row").toArray().map(function(v){var u=e(v);
return{columnId:u.attr("data-column"),cellValue:u.attr("data-cell-value")}
})
}function h(u){return 50*u.data.length+50
}function g(u,v,x,z){var A,y,w;
A={x:d3.mouse(z.$wrapper[0])[0],y:d3.mouse(z.$wrapper[0])[1]};
y=A.x-parseInt(z.infoBox.style("width"),10)/2;
w=A.y-parseInt(z.infoBox.style("height"),10)-10;
z.infoBox.style("left",y+"px").style("top",w+"px")
}function i(u,v,w,x){var y=u[x.options.graphs[0].xAxisColumn];
x.$infoBox=e(x.infoBox[0]);
x.infoBox.style("display","block");
x.$infoBox.find(".js-value").text(n(y))
}function k(u,v,w,x){x.infoBox.style("display","none")
}function m(u){u=parseInt(u,10);
if(isNaN(u)){u=0
}return u
}function r(x,v,w,y){if(typeof y==="undefined"){y=w
}var u=x[y];
if(u&&u.indexOf(",")!==-1){u=u.replace(",","")
}u=m(u);
return u
}function q(w,v){var u=Array.prototype.slice.call(arguments);
u.push(v);
u.shift();
u.shift();
return w.apply(this,u)
}function f(x){var u,w,v;
u=e('<div class="fake axis" />').css({left:"-9999px",opacity:"0",position:"absolute","pointer-events":"none"});
for(w in x.data){if(x.data.hasOwnProperty(w)){u.append("<text>"+j(x.data[w][x.options.graphs[0].yAxisColumn])+"</text>");
if(x.data.length===1){u.append("<text>"+j(x.data[w][x.options.graphs[0].yAxisColumn])+"</text>")
}}}x.$barchart.append(u);
v=x.$barchart.find(".fake").first().children().toArray().reduce(function(y,A){var z={element:A,width:e(A).outerWidth()};
if(z.width>y.width){return z
}else{if(typeof y.width==="undefined"){return z
}else{return y
}}});
return v
}function t(v){var w=v,u;
u={xAccess:q.bind(undefined,r,w.options.graphs[0].xAxisColumn),yAccess:function x(y){return j(y[w.options.graphs[0].yAxisColumn])
},initialHeight:h(v),margin:{}};
u.margin.left=f(v).width+20+5;
v.barChart=new BarChart(w.$barchart[0],u).on("barMouseOver",q.bind(undefined,i,w)).on("barMouseMove",q.bind(undefined,g,w)).on("barMouseOut",q.bind(undefined,k,w));
v.barChart.data(v.data);
v.barchartInit=true;
setTimeout(function(){e(".x.axis text").each(function(y,A){var z=e(A);
z.html(n(z.html()))
})
},100)
}function c(z){var C=[],A,x,B,u,w,v=[];
function y(E,D){return l.options.skipRows.every(function(F){if(F.columnId===E&&F.cellValue===D){return true
}})
}z=z.filter(function(F,D,G){var E=F[l.options.graphs[0].limiting_column];
if(typeof E!=="undefined"&&E===l.options.graphs[0].limiting_cellValue){return true
}return null
});
z.map(function(F,D,G){var E;
for(E in F){if(F.hasOwnProperty(E)){if(y(E,F[E])){C.push(F[E])
}}}});
z=z.filter(function(G,E,H){var F,D;
for(F=C.length-1;
F>=0;
F-=1){for(D=l.options.skipRows.length-1;
F>=0;
F-=1){if(G[l.options.skipRows[D].columnId]===C[F]){return null
}}}return G
});
return z
}function o(v,u){l=u;
l.data=c(v[0]);
l.infoBox=d3.select(l.$infobox[0]);
if(l.$wrapper.height()){t(l)
}else{e(".transparency-tabs").on("change",l,function(w){var x=w.data;
if(!x.barchartInit&&x.$wrapper.height()){t(x)
}})
}}TwitterAEM.components.graphs.bars.init=function(u,w,v){TwitterAEM.components.graphs.bars.initData(function(y,x){o(y,x)
},u,w,v)
};
e.extend(d.prototype,{init:function p(){s();
TwitterAEM.components.graphs.bars.init(l.id,l.$wrapper,l)
}});
e.fn[a]=function(u){return this.each(function(){if(!e.data(this,"plugin_"+a)){e.data(this,"plugin_"+a,new d(this,u))
}})
}
}(jQuery));
$(".tr07-bar-charts .graph-container").each(function(){var a=$(this);
if(a.height()){a.barchart()
}});
$(".tr07-bar-charts").switchChartsFromDropdown({callback:function(a){a.barchart()
}});