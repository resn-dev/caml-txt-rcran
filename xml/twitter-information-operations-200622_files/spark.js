var SparkLine;
(function(){var a=[],d={margin:{top:0,right:0,bottom:0,left:0},offset:[0,0],interoplate:"cardinal",xAccess:function c(g,f){return f
},yAccess:function e(f){return f
},lineWidth:1};
SparkLine=d3Kit.factory.createChart(d,a,function b(l){var i=l.options(),h=l.getDispatcher(),k=l.getRootG().append("path"),g,f,m;
function j(){if(!(l.hasData()&&l.hasNonZeroArea())){return
}var n=l.data();
f.domain(d3.extent(n,i.xAccess));
m.domain(d3.extent(n,i.yAccess));
f.range([0,l.getInnerWidth()]);
m.range([l.getInnerHeight(),0]);
k.datum(n).classed("line",true).attr("d",g).style("fill","none").style("stroke-width",i.lineWidth).style("stroke-linecap","round")
}l.autoResize("both").resizeToFitContainer("both").on("data",j).on("resize",j);
f=d3.scale.linear();
m=d3.scale.linear();
g=d3.svg.line().interpolate(i.interoplate).x(function(o,n){return f(i.xAccess(o,n))
}).y(function(o,n){return m(i.yAccess(o,n))
});
return l.mixin({visualize:j})
})
}());
(function(e){var d="sparkline",c=this,a={};
function b(i,h){c=this;
c.$wrapper=e(i);
c.chartContainer=c.$wrapper.find(".graph")[0];
c.options={data:c.$wrapper.attr("data-value-list")};
e.extend(c.options,h);
c.init()
}function f(){var i,h;
i=new SparkLine(c.chartContainer,{margin:{top:5,right:5,bottom:5,left:5}});
h=c.options.data.split(",").map(Number);
if(h.length===1){h.push(h[0])
}i.data(h)
}e.extend(b.prototype,{init:function g(){f()
}});
e.fn[d]=function(h){return this.each(function(){if(!e.data(this,"plugin_"+d)){e.data(this,"plugin_"+d,new b(this,h))
}})
}
}(jQuery));
$(function(){$(".spark-line-container").sparkline()
});