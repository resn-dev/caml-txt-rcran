# file argument
f=$1
#f=/tmp/caml/china_052020_tweets_csv_hashed.csv
b=$(basename $f .csv)
# directory structure
#d=/tmp/caml/teid
d=/var/gcloud/teid
#rm -rf "$d"
mkdir -p "$d"/lang-en
#
echo "processing file: $f..."

######################################################################## 200830: number of en tweets
if true; then
#d=/var/gcloud/tmp
cat "$f" | \
# select rows, english language 
egrep -c '\s"en"\s' > "$d"/lnum-"$b"
fi

######################################################################## 200827: ikea pilot
if false; then
d=/var/gcloud/ikea
mkdir -p "$d"
#
cat "$f" | \
# select rows, english language 
#egrep '\s"en"\s' | \
# select rows, swedish content
tee -p \
>(egrep -i 'ikea' | shuf | head -n1000 | sed 's/^/"ikea"\t/g' > "$d"/ikea-smp-"$b".csv) \
>(egrep -c -i 'ikea' > "$d"/ikea-agg-"$b".csv) \
>/dev/full
fi

# ikea head
echo -e '"sweet"\t"archive_id"\t"tweet_id"\t"tweet_time"\t"tweet_text"\t"account_language"\t"tweet_language"\t"like_count"\t"user_id"\t"following_count"' > "$d"/ikea-smp-head.csv

######################################################################## 200825: samples
if false; then
cat "$f" | \
# select rows, english language 
egrep '\s"en"\s' | \
# select rows, swedish content
tee -p \
>(egrep -i '(swed|sveri|svensk)' | shuf | head -n1000 | sed 's/^/"sv"\t/g' > "$d"/"$b"-sv.csv) \
>(shuf | head -n1000 | sed 's/^/"rs"\t/g' > "$d"/"$b"-rs.csv) \
>(egrep -v -i '(swed|sveri|svensk)' | shuf | head -n1000 | sed 's/^/"ns"\t/g' > "$d"/"$b"-ns.csv) \
>/dev/full
#>/dev/null
fi

######################################################################## 200820: aggregates
if false; then
#for f in norm/*.csv; do
# accounts, count unique user_id
#b=$(basename $f .csv)
#echo "$b";
#anum=$(tail -n +2 "$f" | cut -f8 | sort | uniq | wc -l)
#echo -e "$b\t$anum" >> tmp/teid-anum.csv
#tail -n +2 "$f" | cut -f1,8 >> ../tmp/teid-anum.csv
#done

#cat "$f" | 
tail -n +2 "$f" | \
# tweet count, sweet count, user count, tweet/account languages
tee -p \
>(wc | tr -s ' ' > "$d"/tnum-"$b") \
>(egrep -c '\s"en"\s' > "$d"/lnum-"$b") \
>(egrep -c -i '(swed|sveri|svensk)' > "$d"/snum-"$b") \
>(cut -f8 | sort | uniq | wc -l > "$d"/unum-"$b") \
>(cut -f5 | sort | uniq -c > "$d"/alang-"$b") \
>(cut -f6 | sort | uniq -c > "$d"/tlang-"$b") \
>/dev/full
fi

######################################################################## 200725: cleaning, not working
if false; then
# extract urls, tags, ausr
#for c in {ns,rs,sv}; do
for c in {sv,}; do
#
echo "processing category: $c"; fin=csv/teid-data-"$c".csv; bo=$(basename "$fin"); > tmp/sed-"$c";

# urls
fou=tmp/teid-urls-"$c".csv; echo "urls" > "$fou"
tail -n +2 "$fin" | while read line; do echo "$line" | egrep -o 'http[^[:space:]]+' | tr '\n' '|'; echo -e ""; done >> "$fou"; echo "done: $fou"
#
tail -n +2 "$fou" | sed -e 's/\//\\\//g' -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g' >> tmp/sed-"$c"

# hashtags
foh=tmp/teid-tags-"$c".csv; echo "tags" > "$foh"
tail -n +2 "$fin" | while read line; do echo "$line" | egrep -o '#[^[:space:]:.,]+' | tr '\n' '|'; echo -e ""; done >> "$foh"; echo "done: $foh"
#
tail -n +2 "$foh" | sed -e 's/\//-/g' -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g' >> tmp/sed-"$c"

# at mention user
foa=tmp/teid-ausr-"$c".csv; echo "ausr" > "$foa"
tail -n +2 "$fin" | while read line; do echo "$line" | egrep -o '@[^[:space:]:.,]+' | tr '\n' '|'; echo -e ""; done >> "$foa"; echo "done: $foa"
#
tail -n +2 "$foa" | sed -e 's/\//-/g' -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g' >> tmp/sed-"$c" 

# combine 
echo "cleaning text: tmp/sed-$c"
paste <(sed -f tmp/sed-"$c" "$fin" | tr -s ' ') <(cat "$fou") <(cat "$foh") <(cat "$foa") > tmp/"$bo"

#
done
fi

if false; then
# delete urls, tags, ausr
#for c in {ns,rs,sv}; do
for c in {sv,}; do
#
fin=tmp/teid-urls-"$c".csv

> tmp/sed-"$c"
#
tail -n +2 "$fou" | sed -e 's/\//\\\//g' -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g' >> tmp/sed-"$c"
#
tail -n +2 "$foh" | sed -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g' >> tmp/sed-"$c"
#
tail -n +2 "$foa" | sed -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g' >> tmp/sed-"$c" 

#
done
#for line in $(tail -n +2 tmp/teid-urls-rs.csv | sed -e 's/\//\\\//g' -e 's/^/s\//g' -e 's/|$//g' -e 's/$/\/\/g/g' -e 's/|/\\|/g' -e 's/s\/\/\/g//g'); do sed -i "$line" some; done
fi


