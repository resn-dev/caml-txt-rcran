# file argument
f=/tmp/caml/2018/10/01/00/29.json
# directory structure
d=/tmp/caml/tiad
rm -rf "$d"
mkdir -p "$d"/lang-en
# testing condition
if true; then
# split tweets
for i in $(seq $(wc -l "$f" | cut -f1 -d' ')); do 
n=$(printf "%05d" $i); sed -n "$i"p "$f" | jq > "$d"/"$n".json; 
done
#
fi
# select languages
for i in "$d"/*.json; do cat "$i" | jq .lang; done | sort | uniq -c
# select english
egrep '^  "lang": "en",' "$d"/*.json | head
egrep '^  "lang": "en",' "$d"/*.json | wc
egrep -l '^  "lang": "en",' "$d"/*.json | head -n100 | xargs -I{} cp {} "$d"/lang-en/
# select fields
cat "$d"/0003.json | jq '{id, timestamp_ms, text, lang}, ({user} | .[] | {id, name, screen_name}) | values[]'
# get 100 row sample, tiad
for i in "$d"/lang-en/*.json; do cat "$i" | jq '{id, timestamp_ms, text, lang}, ({user} | .[] | {id, name, screen_name}) | values[]' | tr '\n' '\t' | sed -e '$a\' >> "$d"/tiad-data.csv; done
#
echo -e "tweet_id\ttweet_time\ttweet_text\ttweet_lang\tuser_id\tuser_name\tuser_screen_name" > ~/dev/r-cran/caml-txt/csv/tiad-head.csv
cp "$d"/tiad-data.csv ~/dev/r-cran/caml-txt/csv/tiad-data.csv
