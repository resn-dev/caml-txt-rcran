#~ ##################################################################### 200602
#install.packages("quanteda", lib="~/lib/r-cran")
library(quanteda)

#~ ##################################################################### 200529
#sfac r workshop
#https://www.kaggle.com/nilsholmberg/scom-komc30/
library(caret)
library(e1071)
library(tm)
library(tidyverse)
library(tidytext)
library(stringr)

#install.packages("pins", lib="~/lib/r-cran")
library(pins)
board_register("kaggle", token="~/doc/env/kaggle/kaggle.json")
#pin(iris, description="some-test", board="kaggle")

#load, explore
#paths <- pins::pin_get("nltkdata/movie-review", "kaggle")
#paths <- pins::pin_get("nilsholmberg/scom-socm", "kaggle")
#read as tibble
paths <- pins::pin_get("datasnaek/mbti-type", "kaggle")
dfm <- readr::read_csv(paths[1])
#dfm <- readr::read_csv("/tmp/caml/mbti_1.csv")

#cols, doc id
dfm <- dfm %>% rename(mbti=type, text=posts)
dfm <- mutate(dfm, id=1:nrow(dfm))
dfm <- dfm %>%
mutate(id=paste0(mbti, "_", id))

#explore types
barplot(table(dfm$mbti))

#filter
dfm <- dfm %>% 
filter(mbti=="ENTP" | mbti=="ENFP" | mbti=="INFP" | mbti=="INFJ")
table(dfm$mbti)

#partition, or at dtm?
mbti_train =
mbti_test =

#clean, alphanumeric 
dfm = dfm %>%
mutate(text=str_replace_all(text, "[^[:alnum:]]", " "))

#tokenize
mbti_tok <- dfm %>%
unnest_tokens(word, text) %>%
filter(str_detect(word, "[a-z']$"),
!word %in% stop_words$word)
###################### alternative
mbti_tok <- dfm %>%
unnest_tokens(output=word, input=text) %>%
# remove numbers
filter(!str_detect(word, "^[0-9]*$")) %>%
# remove stop words
anti_join(stop_words) %>%
# stem the words
mutate(word = SnowballC::wordStem(word))
#
mbti_tok %>%
count(word, sort = TRUE)

#document term matrix, term frequency weighting
mbti_dtm <- mbti_tok %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf weighting
cast_dtm(document=id, term=word, value=n)

#weighting, tf-idf
mbti_tok %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf-idf weighting
cast_dtm(document=id, term=word, value=n, weighting=tm::weightTfIdf)

#sparsity, level?
removeSparseTerms(mbti_dtm, sparse=.95)

#salient words
mbti_tfidf <- mbti_tok %>%
count(mbti, word) %>%
bind_tf_idf(term=word, document=mbti, n=n)

#plot
# sort the data frame and convert word to a factor column
mbti_plot <- mbti_tfidf %>%
arrange(desc(tf_idf)) %>%
mutate(word = factor(word, levels = rev(unique(word))))
# graph the top 10 tokens for 4 categories
mbti_plot %>%
filter(mbti %in% c(
"ENTP",
"ENFP",
"INFP", 
"INFJ")) %>%
group_by(mbti) %>%
top_n(10) %>%
ungroup() %>%
mutate(word = reorder_within(word, tf_idf, mbti)) %>%
ggplot(aes(word, tf_idf)) +
geom_col() +
scale_x_reordered() +
labs(x = NULL, y = "tf-idf") +
facet_wrap(~ mbti, scales = "free") +
coord_flip()


# dtm to df
mbti_df = as.data.frame(as.matrix(mbti_dtm))
dim(mbti_df)
mbti_df$mbti = rownames(mbti_df)
mbti_df$mbti = gsub("_.*", "", mbti_df$mbti)

#partition data, nb needs same features
set.seed(99)
#train
train_idx <- caret::createDataPartition(mbti_df$mbti, p=0.7)$Resample1
train_data <- mbti_df[train_idx, ]
barplot(table(train_data$mbti))
train_labs = train_data$mbti
train_data = train_data[ , -which(names(train_data) %in% c("mbti"))]
#model
m <- naiveBayes(x=train_data, y=as.factor(train_labs), laplace=1.0)
#test
test_data <- mbti_df[-train_idx, ]
barplot(table(test_data$mbti))
test_labs = test_data$mbti
test_data = test_data[ , -which(names(test_data) %in% c("mbti"))]
#predict
p = predict(m, test_data)
#evaluate
table(predict(m, test_data), test_labs)
cfm <- caret::confusionMatrix(data=p, as.factor(test_labs))





#~ ##################################################################### 200529



#rm -Rf comp.os.ms-windows.misc comp.sys.ibm.pc.hardware comp.sys.mac.hardware comp.windows.x rec.motorcycles rec.sport.hockey sci.crypt soc.religion.christian talk.politics.misc talk.religion.misc



fp <- "/tmp/caml/20news-bydate-train/"
tng_dtm = get_dtm(fp)
#
tng_df = as.data.frame(as.matrix(tng_dtm))
tng_df$newsgroup = rownames(tng_df)
tng_df$newsgroup = gsub("_.*", "", tng_df$newsgroup)

#
#m <- naiveBayes(x=as.matrix(tng_train), y=as.factor(as.matrix(tng_lab)), laplace=0.5)

set.seed(99)
#
trainIndex <- caret::createDataPartition(tng_df$newsgroup, p=0.7)$Resample1
train_data <- tng_df[trainIndex, ]
barplot(table(train_data$newsgroup))
train_labs = train_data$newsgroup
train_data = train_data[ , -which(names(train_data) %in% c("newsgroup"))]
#
test_data <- tng_df[-trainIndex, ]
test_labs = test_data$newsgroup
test_data = test_data[ , -which(names(test_data) %in% c("newsgroup"))]
#
m <- naiveBayes(x=train_data, y=as.factor(train_labs), laplace=1.0)
p = predict(m, test_data)
#
table(predict(m, test_data), test_labs)
cfm <- caret::confusionMatrix(data=p, as.factor(test_labs))







#~ ##################################################################### 200529
#sfac r workshop
#http://qwone.com/~jason/20Newsgroups/20news-bydate.tar.gz
library(caret)
library(e1071)
library(tm)
library(tidyverse)
library(tidytext)
library(stringr)
#library(textmineR)

#clear workspace
rm(list=ls())
#import trial dataset, 1920 cases
#dft = read.table('csv/dataset-trials-200218.csv', sep='\t', header=T, strip.white=TRUE, colClasses=c("com_view"="character"))
#load functions
source("src/multiclass-functions.R")

fp <- "/tmp/caml/20news-bydate-train/"
tng_train = get_dtm(fp)
#
tng_lab = tibble(docs=tng_train$dimnames$Docs) %>%
separate(docs, c("newsgroup"), sep="_", remove=T)
#
m <- naiveBayes(x=as.matrix(tng_train), y=as.factor(as.matrix(tng_lab)), laplace=0.5)


fp <- "/tmp/caml/20news-bydate-test/"
tng_test = get_dtm(fp)
#
p = predict(m, as.matrix(tng_test))




data(iris)
set.seed(99)
#
trainIndex <- caret::createDataPartition(iris$Species, p=0.7)$Resample1
train_data <- iris[trainIndex, ]
test_data <- iris[-trainIndex, ]
#
m <- naiveBayes(x=train_data[,-5], y=as.factor(train_data[,5]), laplace=0.5)
p = predict(m, test_data)
#
table(predict(m, test_data), test_data[,5])
cfm <- caret::confusionMatrix(data=p, test_data[,5])



###################### 

#read text data
training_folder <- "/tmp/caml/20news-bydate-train/"
fp <- "/tmp/caml/20news-bydate-test/"
# Use unnest() and map() to apply read_folder to each subfolder
tng_dt <- tibble(folder = dir(training_folder, full.names = TRUE)) %>%
mutate(folder_out = map(folder, read_folder)) %>%
unnest(cols = c(folder_out)) %>%
transmute(newsgroup = basename(folder), id, text)
#get unique ids, 11314
tng_dt = mutate(tng_dt, id=paste(newsgroup, id, sep="_"))
#group by id, concat text
tng_dt <- tng_dt %>%
group_by(id) %>%
summarise(text = toString(text)) %>%
ungroup()
#create newsgroup labels
tng_dt <- tng_dt %>%
#mutate(newsgroup=unlist(str_split(id, "-", n=2, simplify=FALSE))[1])
separate(id, c("newsgroup"), sep="_", remove=F)


#explore
tng_dt %>%
group_by(newsgroup) %>%
summarize(messages = n_distinct(id)) %>%
ggplot(aes(newsgroup, messages)) +
geom_col() +
coord_flip()

#clean
# must occur after the first occurrence of an empty line,
# and before the first occurrence of a line starting with --
#tng_dt <- tng_dt %>%
#group_by(newsgroup, id) %>%
#filter(cumsum(text == "") > 0,
#cumsum(str_detect(text, "^--")) == 0) %>%
#ungroup()
#
tng_dt <- tng_dt %>%
filter(str_detect(text, "^[^>]+[A-Za-z\\d]") | text == "",
!str_detect(text, "writes(:|\\.\\.\\.)$"),
!str_detect(text, "^In article <"),
!id %in% c(9704, 9985))
#alphanumeric 
tng_dt = tng_dt %>%
mutate(text=str_replace_all(text, "[^[:alnum:]]", " "))

#tokenize
tng_tok <- tng_dt %>%
unnest_tokens(word, text) %>%
filter(str_detect(word, "[a-z']$"),
!word %in% stop_words$word)
###################### alternative
tng_tok <- tng_dt %>%
unnest_tokens(output=word, input=text) %>%
# remove numbers
filter(!str_detect(word, "^[0-9]*$")) %>%
# remove stop words
anti_join(stop_words) %>%
# stem the words
mutate(word = SnowballC::wordStem(word))
#
tng_tok %>%
count(word, sort = TRUE)


#document term matrix, term frequency weighting
tng_dtm <- tng_tok %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf weighting
cast_dtm(document=id, term=word, value=n)

#weighting, tf-idf
tng_tok %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf-idf weighting
cast_dtm(document=id, term=word, value=n, weighting=tm::weightTfIdf)

#sparsity
removeSparseTerms(tng_dtm, sparse=.98)


#salient words
tng_tfidf <- tng_tok %>%
count(newsgroup, word) %>%
bind_tf_idf(term=word, document=newsgroup, n=n)


#plot
# sort the data frame and convert word to a factor column
tng_plot <- tng_tfidf %>%
arrange(desc(tf_idf)) %>%
mutate(word = factor(word, levels = rev(unique(word))))
# graph the top 10 tokens for 4 categories
tng_plot %>%
filter(newsgroup %in% c(
"alt.atheism",
"comp.graphics",
"rec.motorcycles", 
"talk.politics.guns")) %>%
group_by(newsgroup) %>%
top_n(10) %>%
ungroup() %>%
mutate(word = reorder_within(word, tf_idf, newsgroup)) %>%
ggplot(aes(word, tf_idf)) +
geom_col() +
scale_x_reordered() +
labs(x = NULL, y = "tf-idf") +
facet_wrap(~ newsgroup, scales = "free") +
coord_flip()

#dtm to df, bow, bag of words? 
tng_bow <- tidy(tng_dtm)


#partition
#set.seed(99)
#tng_idx <- caret::createDataPartition(tng_dtm$dimnames$Docs, p=0.7)$Resample1
#train_data <- corpus_as_dtm[trainIndex, ]
#test_data <- corpus_as_dtm[-trainIndex, ] 

m <- naiveBayes(x=as.matrix(tng_dtm), y=as.factor(tng_dt$newsgroup), laplace=0.5)



data(iris)
m <- naiveBayes(x=iris[,-5], y=as.factor(iris[,5]), laplace=0.5)
table(predict(m, iris), iris[,5])
p = predict(m, iris)
cfm <- caret::confusionMatrix(data=p, iris[,5])

#~ ##################################################################### 200528
#mbti dataset
library(caret)
library(tm)
library(tidyverse)
library(tidytext)
library(stringr)
library(textmineR)

#install.packages("pins", lib="~/lib/r-cran")
library(pins)
board_register("kaggle", token="~/doc/env/kaggle/kaggle.json")
#pin(iris, description="some-test", board="kaggle")

#load, explore
#paths <- pins::pin_get("nltkdata/movie-review", "kaggle")
#paths <- pins::pin_get("nilsholmberg/scom-socm", "kaggle")
#read as tibble
paths <- pins::pin_get("datasnaek/mbti-type", "kaggle")
dfm <- readr::read_csv(paths[1])
dfm <- readr::read_csv("/tmp/caml/mbti_1.csv")
#cols
dfm <- dfm %>% rename(mbti=type, text=posts)
dfm <- mutate(dfm, id=1:nrow(dfm))

#partition, or at dtm?
mbti_train =
mbti_test =

#explore
barplot(table(dfm$mbti))

#clean, alphanumeric 
dfm = dfm %>%
mutate(text=str_replace_all(text, "[^[:alnum:]]", " "))

#tokenize
#library(tidytext)
mbti_tok <- dfm %>%
unnest_tokens(word, text) %>%
filter(str_detect(word, "[a-z']$"),
!word %in% stop_words$word)

###################### alternative
mbti_tok <- dfm %>%
unnest_tokens(output=word, input=text) %>%
# remove numbers
filter(!str_detect(word, "^[0-9]*$")) %>%
# remove stop words
anti_join(stop_words) %>%
# stem the words
mutate(word = SnowballC::wordStem(word))
#
mbti_tok %>%
count(word, sort = TRUE)


#document term matrix, term frequency weighting
mbti_dtm <- mbti_tok %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf weighting
cast_dtm(document=id, term=word, value=n)

#weighting, tf-idf
mbti_tok %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf-idf weighting
cast_dtm(document=id, term=word, value=n, weighting=tm::weightTfIdf)

#sparsity
removeSparseTerms(mbti_dtm, sparse=.95)


#uncommon words
mbti_tfidf <- mbti_tok %>%
count(mbti, word) %>%
bind_tf_idf(term=word, document=mbti, n=n)


#plot
# sort the data frame and convert word to a factor column
mbti_plot <- mbti_tfidf %>%
arrange(desc(tf_idf)) %>%
mutate(word = factor(word, levels = rev(unique(word))))
# graph the top 10 tokens for 4 categories
mbti_plot %>%
filter(mbti %in% c(
"ENTP",
"ENFP",
"INFP", 
"INFJ")) %>%
group_by(mbti) %>%
top_n(10) %>%
ungroup() %>%
mutate(word = reorder_within(word, tf_idf, mbti)) %>%
ggplot(aes(word, tf_idf)) +
geom_col() +
scale_x_reordered() +
labs(x = NULL, y = "tf-idf") +
facet_wrap(~ mbti, scales = "free") +
coord_flip()






#~ ##################################################################### 200525
#20 newsgroups dataset
#install.packages(c("pins","tm","caret","e1071","caTools"), lib="~/lib/r-cran")
#install.packages(c("tidytext"), lib="~/lib/r-cran")

#naive bayes classification
library(e1071)
library(caTools)
#install.packages("naivebayes", lib="~/lib/r-cran")
library(naivebayes)
#devtools::install_github("jaytimm/quicknews")

#
library(glmnet)

#random forest, svm
library(caret)
library(tm)
library(tidyverse)
library(tidytext)
library(stringr)

#clear workspace
rm(list=ls())
#import trial dataset, 1920 cases
#dft = read.table('csv/dataset-trials-200218.csv', sep='\t', header=T, strip.white=TRUE, colClasses=c("com_view"="character"))
#load functions
source("src/multiclass-functions.R")


#read text data
training_folder <- "/tmp/caml/20news-bydate-train/"
# Use unnest() and map() to apply read_folder to each subfolder
dtm <- tibble(folder = dir(training_folder, full.names = TRUE)) %>%
mutate(folder_out = map(folder, read_folder)) %>%
unnest(cols = c(folder_out)) %>%
transmute(newsgroup = basename(folder), id, text)


#explore
#library(ggplot2)
dtm %>%
group_by(newsgroup) %>%
summarize(messages = n_distinct(id)) %>%
ggplot(aes(newsgroup, messages)) +
geom_col() +
coord_flip()

#clean
#library(stringr)
# must occur after the first occurrence of an empty line,
# and before the first occurrence of a line starting with --
dtm <- dtm %>%
group_by(newsgroup, id) %>%
filter(cumsum(text == "") > 0,
cumsum(str_detect(text, "^--")) == 0) %>%
ungroup()
#
dtm <- dtm %>%
filter(str_detect(text, "^[^>]+[A-Za-z\\d]") | text == "",
!str_detect(text, "writes(:|\\.\\.\\.)$"),
!str_detect(text, "^In article <"),
!id %in% c(9704, 9985))
#alphanumeric 
dtm = dtm %>%
mutate(text=str_replace_all(text, "[^[:alnum:]]", " "))


#tokenize
#library(tidytext)
usenet_words <- dtm %>%
unnest_tokens(word, text) %>%
filter(str_detect(word, "[a-z']$"),
!word %in% stop_words$word)

###################### alternative
usenet_words <- dtm %>%
unnest_tokens(output=word, input=text) %>%
# remove numbers
filter(!str_detect(word, "^[0-9]*$")) %>%
# remove stop words
anti_join(stop_words) %>%
# stem the words
mutate(word = SnowballC::wordStem(word))
#
usenet_words %>%
count(word, sort = TRUE)


#document term matrix, term frequency weighting
usenet_dtm <- usenet_words %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf weighting
cast_dtm(document=id, term=word, value=n)

#weighting, tf-idf
usenet_words %>%
# get count of each token in each document
count(id, word) %>%
# create a document-term matrix with all features and tf-idf weighting
cast_dtm(document=id, term=word, value=n, weighting=tm::weightTfIdf)

#sparsity
usenet_dtm = removeSparseTerms(usenet_dtm, sparse=.99)


#uncommon words
usenet_tfidf <- usenet_words %>%
count(newsgroup, word) %>%
bind_tf_idf(term=word, document=newsgroup, n=n)


#plot
# sort the data frame and convert word to a factor column
usenet_plot <- usenet_tfidf %>%
arrange(desc(tf_idf)) %>%
mutate(word = factor(word, levels = rev(unique(word))))
# graph the top 10 tokens for 4 categories
usenet_plot %>%
filter(newsgroup %in% c(
"alt.atheism",
"comp.graphics",
"rec.motorcycles", 
"talk.politics.guns")) %>%
group_by(newsgroup) %>%
top_n(10) %>%
ungroup() %>%
mutate(word = reorder_within(word, tf_idf, newsgroup)) %>%
ggplot(aes(word, tf_idf)) +
geom_col() +
scale_x_reordered() +
labs(x = NULL, y = "tf-idf") +
facet_wrap(~ newsgroup, scales = "free") +
coord_flip()




#~ ##################################################################### 200401
library(tm)
library(tibble)

docs <- c('Cats like to chase mice.', 'Dogs bite people.', 'Dogs like to run after cats.')
str(docs); class(docs); typeof(docs)
# Create a corpus from our character vector
corp <- Corpus(VectorSource(docs))
# Create the document term matrix
dtm <- DocumentTermMatrix(corp, list(
removePunctuation = TRUE, 
removeNumbers = TRUE))



#
train_set <- as.matrix(dtm)

#syntactically valid colnames, i.e. not beginning with number etc
colnames(train_set) = make.names(colnames(train_set))


# add the classifier column and make it a data frame
train_set <- cbind(train_set, c(0,1,0))
colnames(train_set)[ncol(train_set)] <- "y"
train_set <- as.data.frame(train_set)
train_set$y <- as.factor(train_set$y)







###################### 
#url to workshop: https://www.kaggle.com/nilsholmberg/scom-komc30


devtools::session_info()
#~ ##################################################################### 200331
library(dplyr)
library(ggplot2)
library(purrr)
#install.packages("pins", lib="~/lib/r-cran")
library(pins)
board_register("kaggle", token="~/doc/env/kaggle/kaggle.json")
#pin(iris, description="some-test", board="kaggle")

#load, explore
#paths <- pins::pin_get("nltkdata/movie-review", "kaggle")
#paths <- pins::pin_get("nilsholmberg/scom-socm", "kaggle")
#USCongress
paths <- pins::pin_get("datasnaek/mbti-type", "kaggle")
path <- paths[1]
df <- readr::read_csv(path)

###################### dtm, document-term matrix
library(tidyverse)
library(tidytext)
library(stringr)
#install.packages("caret", lib="~/lib/r-cran")
library(caret)
library(tm)
#install.packages("textmineR", lib="~/lib/r-cran")
library(textmineR)
set.seed(24130)

#dtm tm
data <- c('Cats like to chase mice.', 'Dogs like to eat big bones.')
corpus <- VCorpus(VectorSource(data))
#
dtm <- DocumentTermMatrix(corpus, list(
removePunctuation = TRUE, 
tolower=T,
stopwords = TRUE, 
stemming = TRUE, 
removeNumbers = TRUE),
stripWhitespace=T)
#tf-idf weighting
#remove sparse
dtm <- removeSparseTerms(dtm, 0.4)

#dtm RTextTools
#removed from cran as of 190305

#dtm quanteda
library(quanteda)
txts <- c('Cats like to chase mice.', 'Dogs like to eat big bones.')
corp = corpus(txts)
tbt = tokens(corp, remove_punct = TRUE)
tbt = tokens_remove(tbt, pattern = stopwords('en'))
#
dtm = dfm(tbt)
dfm_tfidf(dtm)

#dtm tidytext
data <- c('Cats like to chase mice.', 'Dogs like to eat big bones.')
#tibble with line and text named fields
tbt <- tibble(line=1:2, text=data) %>% 
#tokenize
unnest_tokens(output = word, input = text) %>%
# remove numbers
filter(!str_detect(word, "^[0-9]*$")) %>%
# remove stop words
anti_join(stop_words) %>%
# stem the words
mutate(word = SnowballC::wordStem(word))
#
dtm <- tbt %>%
# get count of each token in each document
count(line, word) %>%
# create a document-term matrix with all features and tf weighting
cast_dtm(document = line, term = word, value = n)
#tf-idf weighting
tbt %>%
# get count of each token in each document
count(line, word) %>%
# create a document-term matrix with all features and tf-idf weighting
cast_dtm(document = line, term = word, value = n,
weighting = tm::weightTfIdf)
#use tokens to calculate tfidf
tfidf <- tbt %>%
count(line, word) %>%
bind_tf_idf(term = word, document = line, n = n)
# make sparse
dtm <- removeSparseTerms(dtm, sparse = .99)

#dtm textmineR
data <- c('Cats like to chase mice.', 'Dogs like to eat big bones.')
#
dtm <- CreateDtm(doc_vec = data, # character vector of documents
doc_names = seq(length(data)), # document names, optional
#ngram_window = c(1, 2), # minimum and maximum n-gram length
#stopword_vec = c(stopwords::stopwords("en")), # stopwords from tm
stopword_vec = c(stopwords::stopwords(source = "smart")), # this is the default value
lower = TRUE, # lowercase - this is the default value
remove_punctuation = TRUE, # punctuation - this is the default
remove_numbers = TRUE, # numbers - this is the default
verbose = FALSE, # Turn off status bar for this demo
cpus = 2, # by default, this will be the max number of cpus available
stem_lemma_function = function(x) SnowballC::wordStem(x, "porter"))



###################### tcm, term co-occurrence matrix

#tcm textmineR
tcm = Dtm2Tcm(dtm)


#~ ##################################################################### 200309
#
library(caret)
library(tm)

# Training data.
data <- c('Cats like to chase mice.', 'Dogs like to eat big bones.')
corpus <- VCorpus(VectorSource(data))

# Create a document term matrix.
tdm <- DocumentTermMatrix(corpus, list(removePunctuation = TRUE, stopwords = TRUE, stemming = TRUE, removeNumbers = TRUE))

# Convert to a data.frame for training and assign a classification (factor) to each document.
train <- as.matrix(tdm)
train <- cbind(train, c(0, 1))
colnames(train)[ncol(train)] <- 'y'
train <- as.data.frame(train)
train$y <- as.factor(train$y)

# Train.
fit <- train(y ~ ., data = train, method = 'bayesglm')

# Check accuracy on training.
predict(fit, newdata = train)

# Test data.
data2 <- c('Bats eat bugs.')
corpus <- VCorpus(VectorSource(data2))
tdm <- DocumentTermMatrix(corpus, control = list(dictionary = Terms(tdm), removePunctuation = TRUE, stopwords = TRUE, stemming = TRUE, removeNumbers = TRUE))
test <- as.matrix(tdm)

# Check accuracy on test.
predict(fit, newdata = test)
