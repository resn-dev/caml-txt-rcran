#file path
f=$1
# directory structure
#d=/tmp/caml/teid
d=/var/gcloud
mkdir -p "$d"/part
#base
b=$(basename $f .csv)

#max size 1.8G
ms=1800000000
#file size
fs=$(stat -c%s "$f")
du -hs "$f"

#if ((fs > 5GB))
#for f in "$d"/csv/

#if true; then
if (( fs > ms )); then
echo "split ..."
#normalized lines
egrep -n '^"[^[:space:]]{8,50}' "$f" | cut -f1 -d':' > nl.txt
#normalized line count
#lc=$(egrep -n '^"[^[:space:]]{8,50}' "$f" | wc -l)
lc=$(wc -l nl.txt | cut -f1 -d' ')
#cut off line at 1/3
#cl=$(echo $lc*0.6 | bc -l | cut -f1 -d'.')
cl1=$(echo $lc / 3 | bc)
cl2=$((cl1 * 2))
#split lines
#sl=$(egrep -n '^"[^[:space:]]{8,50}' "$f" | tail -n +"$cl" | head -n1 | cut -f1 -d':')
#echo "at line ... $sl"
#sl=$((sl - 1))
#split file
#split -l "$sl" --numeric-suffixes "$f" "$f"-part-
t1=$(sed -n "$cl1"p nl.txt)
s=1;e=$((t1 - 1));
echo "$s,$e"p
sed -n "$s,$e"p "$f" > "$f"-part-1
#
t2=$(sed -n "$cl2"p nl.txt)
s=$t1;e=$((t2 - 1));
echo "$s,$e"p
sed -n "$s,$e"p "$f" > "$f"-part-2
#
tail -n +"$t2" "$f" > "$f"-part-3
#move split files
mv "$f"-part-* "$d"/part/
#
rm nl.txt
else
echo "pass ..."
fi
