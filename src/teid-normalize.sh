# file argument
f=$1
#f=/tmp/caml/china_052020_tweets_csv_hashed.csv
b=$(basename $f .csv)
# directory structure
#d=/tmp/caml/teid
d=/var/gcloud/teid
#rm -rf "$d"
mkdir -p "$d"/lang-en
mkdir -p "$d"/norm
#
echo "processing file: $f..."
# normalize twitter election integrity text data
cat "$f" | \
# filter out extremly long lines
#awk 'length($0)<2600' "$f" | \
# replace cr, \r, e.g. with:
tr -d '\r' | \
# squeeze spaces
tr -s ' ' | \
# squeeze newlines
tr -s '\n' | \
# replace all instances of new reserved quoting character (pipe sign)
sed 's/|/&pipe;/g' | \
# quote line start
sed 's/^"\(tweetid\|[0-9]\+\)/|\1/g' | \
# quote line end
sed 's/\(","[^"]*\)"$/\1|/g' | \
# quote all fields, use tab separator
sed 's/","/|\t|/g' | \
# squeeze, replace standard quoting character (")
sed 's/"\+/&quot;/g' | \
# replace newlines in text fields
sed ':a;N;$!ba;s/\([^|]\)\n/\1 /g' | \
# replace temporary quote character with standard
sed 's/|/"/g' | \
# select, reorder fields
awk -F '\t' '{print $1 "\t" $14 "\t" $13 "\t" $11 "\t" $12 "\t" $26 "\t" $2 "\t" $9}' | \
# add fields, archive_id (10 fields)
sed "s/^/\"$b\"\t/g" | \
########################################################################
#output normalized data
cat > "$d"/norm/"$b".csv

# ensure unique ids across each archive file, e.g. combine md5(archive_id) and tweet_id

# teid head
echo -e '"sweet"\t"archive_id"\t"tweet_id"\t"tweet_time"\t"tweet_text"\t"account_language"\t"tweet_language"\t"like_count"\t"user_id"\t"following_count"' > "$d"/norm/teid-head.csv
#cp "$d"/teid-data.csv ~/dev/r-cran/caml-txt/csv/teid-data.csv

# samples, aggregates refactored to teid-extract.sh (200826)
########################################################################
# aggregate all rows by archive (total number of tweets, swe mentions, tweet/account languages)
#tee -p \
#>(wc | tr -s ' ' > "$d"/tnum-"$b") \
#>(egrep -c -i '(swed|sveri|svensk)' > "$d"/snum-"$b") \
#>(cut -f5 | sort | uniq -c > "$d"/alang-"$b") \
#>(cut -f6 | sort | uniq -c > "$d"/tlang-"$b") \
#>/dev/full
########################################################################
# select rows, english language 
#egrep '\s"en"\s' | \
# select rows, swedish content
#tee -p \
#>(egrep -i '(swed|sveri|svensk)' | shuf | head -n1000 | sed 's/^/"sv"\t/g' > "$d"/"$b"-sv.csv) \
#>(shuf | head -n1000 > "$d"/"$b"-rs.csv) \
#>(egrep -v -i '(swed|sveri|svensk)' | shuf | head -n1000 | sed 's/^/"ns"\t/g' > "$d"/"$b"-ns.csv) \
#>/dev/full
#>/dev/null
########################################################################
