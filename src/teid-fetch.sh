#google auth
gcloud auth activate-service-account --key-file=lib/caml-scom-1acc9d2054d5.json

# set up workspace
d="/tmp/caml/tget"
d="/var/gcloud/tget"
mkdir -p "$d"
cd "$d"

#sed -n '1p;5p' /home/sol-nhl/dev/r-cran/caml-txt/csv/teid-file.csv | 
sed -n '1,30p;31p' /var/gcloud/caml-txt-rcran/csv/teid-file.csv | \
while IFS=$'\t' read s f; do 
b=$(basename "$f" .zip)
z=$(basename "$f")
g=$(echo "$f" | sed 's/h.*\.com/gs:\//')

# get dataset
echo "downloading: $z... file size: $s"; 
#wget "$f" -O/tmp/caml/"$b"
#curl --silent -o "/tmp/caml/$b" -L "$f"
if gsutil cp "$g" "$z"; then
#if false; then

# unpack
unzip -o "$z"

# normalize, subset
echo "normalizing and subsetting twitter data..."
#bash /home/sol-nhl/dev/r-cran/caml-txt/src/teid-normalize.sh "$d"/"$b".csv
echo "$z" >> arch.txt

#delete files
#rm "$z" "$b".csv

else
echo "download failed for file: $g" 
echo "$z" >> fail.txt
fi
#

echo "done: $z"
#done <<<$(sed -n '1,30p;31p' /home/sol-nhl/dev/r-cran/caml-txt/csv/teid-file.csv)
#done <<<$(sed -n '1p;5p' /var/gcloud/caml-txt-rcran/csv/teid-file.csv)
done
echo "done all files"
#
cd -














